###Metrics in an AspectJ & Spring Boot environment
Contains several Codahale related modules such as:

1. jvm
1. sigar
1. statsd
1. servlet

The naming strategy of these modules is not configurable and is always based
on the FQCN. Downsides are:

1. Hard to navigate
1. Loses existing history when package names changes
1. Does not enforce a particular naming strategy
1. Does not hide Codadahale specific implementation

###Codahale Downsides
This project attempts to fix these downsides through:

1. Many GaugeSet and MetricSet have their static MetricRegistry.name() call replaced
with a customizable MetricKeyBuilder and NamespaceResolver implementation where by default
stats are stored under "fixed" keys instead of a FQCN:

    1. <namespace>.os.fs.openfiles
    1. <namespace>.jvm.gc.<gc-collector>.count
    1. <namespace>.jvm.gc.<gc-collector>.time
    1. <namespace>.jvm.classloader.loaded
    1. <namespace>.jvm.classloader.unloaded
    1. <namespace>.jvm.bufferpool.direct.count
    1. <namespace>.jvm.bufferpool.direct.used
    1. <namespace>.jvm.bufferpool.direct.capacity
    1. <namespace>.jvm.bufferpool.mapped.count
    1. <namespace>.jvm.bufferpool.mapped.used
    1. <namespace>.jvm.bufferpool.mapped.capacity
    1. <namespace>.jvm.memory.init
    1. <namespace>.jvm.memory.used
    1. <namespace>.jvm.memory.max
    1. <namespace>.jvm.memory.committed
    1. <namespace>.jvm.thread.<thead-state>.count
    1. <namespace>.jvm.thread.count
    1. <namespace>.jvm.thread.daemon.count
    1. <namespace>.jvm.thread.deadlocks
    1. <namespace>.log.level.all
    1. <namespace>.log.level.trace
    1. <namespace>.log.level.debug
    1. <namespace>.log.level.info
    1. <namespace>.log.level.warn
    1. <namespace>.log.level.error
    1. <namespace>.log.level.fatal

1. Replaces Codahale's existing MetricFilter with a CodahaleMetricsFilter where stats are 
stored under "fixed" keys instead of a FQCN:

    1. <namespace>.http.request.other
    1. <namespace>.http.request.activeRequests
    1. <namespace>.http.request.requests
    1. <namespace>.http.status.<http-code>

1. All metrics related code is hidden away through a Metrics interface with a 
single CodahaleMetricsFacade implementation.
1. Features a Statsd UDP reporter that aggregates stats over X seconds, yet  
won't exceed a particular MTU. It does this by splitting large stats packages into 
multiple packages by aiming around 1024 bytes.
1. Features Sigar auto-detection creating default stats keys:
    
    1. <namespace>.fs.<harddisk-name>.percentage-full
    1. <namespace>.cpu.total-cores
    1. <namespace>.cpu.physical-cpus
    1. <namespace>.cpu.cpu-time-user-percent
    1. <namespace>.cpu.cpu-time-sys-percent
    1. <namespace>.memory.memory-free
    1. <namespace>.memory.memory-actual-free
    1. <namespace>.memory.memory-used
    1. <namespace>.memory.memory-actual-used
    1. <namespace>.memory.memory-total
    1. <namespace>.memory.memory-used-percent
    1. <namespace>.memory.memory-free-percent
    1. <namespace>.memory.swap-free
    1. <namespace>.memory.swap-pages-in
    1. <namespace>.memory.swap-pages-out
    1. <namespace>.ulimit.ulimit-open-files
    1. <namespace>.ulimit.ulimit-stack-size
    1. <namespace>.pid

### Note:
1. Requires Aspect LTW to work
1. Requires Spring Boot to work
1. Includes Sigar native libraries
1. Enable through @MetricsConfiguration