package tix.metrics;

import org.apache.commons.lang3.exception.ExceptionUtils;

public class ExceptionMetricsKey implements MetricsKey {
    private static final String metricsName = "exception";

    private final MetricsKey metricsKey;

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    public ExceptionMetricsKey(Throwable cause) {
        Throwable potentialRootCause = ExceptionUtils.getRootCause(cause);
        Throwable rootCause = potentialRootCause != null ? potentialRootCause : cause;

        this.metricsKey = MetricsKeyBuilder
                .create()
                .withInstrumentedSection(metricsName)
                .withTarget(rootCause.getClass().getSimpleName())
                .build();
    }

    @Override
    public String success() {
        return metricsKey.success();
    }

    @Override
    public String error() {
        return metricsKey.success();
    }
}
