package tix.metrics;

import org.springframework.beans.factory.DisposableBean;
import tix.metrics.time.Duration;
import tix.metrics.time.Interval;

import java.util.concurrent.Callable;

/**
 * ALL methods are NON-blocking and is guaranteed not to throw an exception.
 *
 * Also have a look at:
 * <ol>
 *     <li> http://matt.aimonetti.net/posts/2013/06/26/practical-guide-to-graphite-monitoring/ </li>
 *     <li> http://metrics.codahale.com/manual/core/ </li>
 * </ol>
 */
public interface Metrics extends DisposableBean {
    /**
     * Adjusts the specified counter by a given delta.
     *
     * @param key the name of the counter to adjust
     * @param delta the amount to adjust the counter by
     */
    Metrics count(MetricsKey key, int delta);

    /**
     * Increments the specified counter by one.
     * @param key the key of the counter to decrement
     * @see #meter(MetricsKey)
     */
    Metrics inc(MetricsKey key);

    /**
     * Decrements the specified counter by one.
     * @param key the key of the counter to decrement
     */
    Metrics dec(MetricsKey key);

    /**
     * Records the latest fixed value for the specified named gauge.
     *
     * @param key the name of the gauge @param value the new reading of the gauge
     * @param value the new reading of the gauge
     */
    <V> Metrics gauge(MetricsKey key, Interval interval, V value);

    /**
     * Records the latest fixed value for the specified named gauge.
     *
     * @param key the name of the gauge @param value the new reading of the gauge
     * @param function the new reading of the gauge, which is evaluated every <param>interval</param>
     */
    <V> Metrics gauge(MetricsKey key, Interval interval, Callable<V> function);

    /**
     * Records an execution time in milliseconds for the specified named operation.
     * @param key the key of the timed operation
     * @param duration the duration of a specific call / process
     */
    Metrics time(MetricsKey key, Duration duration);

    /**
     * A metric which calculates the distribution of a value (Histogram).
     *
     * @see <a href="http://www.mathsisfun.com/data/histograms.html"> Simple explanation on Histogram</a>
     * @see <a href="http://www.johndcook.com/standard_deviation.html">Accurately computing running variance</a>
     */
    Metrics histogram(MetricsKey key, int value);

    /**
     * A meter metric which measures mean throughput and one-, five-, and fifteen-minute
     * exponentially-weighted moving average through-puts
     */
    Metrics meter(MetricsKey key);

    /**
     * Records an error using <param>metricsKey</param>, typically
     * by doing an increment on {@link MetricsKey#error()}
     */
    Metrics error(MetricsKey metricsKey);
}
