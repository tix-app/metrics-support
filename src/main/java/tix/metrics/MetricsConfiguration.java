package tix.metrics;

import ch.qos.logback.classic.Logger;
import com.codahale.metrics.MetricFilter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.ScheduledReporter;
import com.codahale.metrics.graphite.*;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.LoadTimeWeavingConfiguration;
import org.springframework.web.WebApplicationInitializer;
import tix.metrics.codahale.CodahaleMetricsFacade;
import tix.metrics.codahale.Statsd;
import tix.metrics.codahale.StatsdReporter;
import tix.metrics.codahale.collector.*;
import tix.metrics.codahale.web.CodahaleMetricsFilter;
import tix.metrics.namespace.NamespaceResolver;
import tix.metrics.namespace.UsernameBasedNamespaceResolver;
import tix.metrics.sigar.SigarMetrics;
import tix.metrics.sigar.detect.NativeSigarLoader;
import tix.metrics.sigar.detect.OsAwareNativeSigarLoader;

import javax.servlet.Filter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.util.concurrent.TimeUnit.SECONDS;

@Slf4j
@Configuration
@AutoConfigureAfter(LoadTimeWeavingConfiguration.class)
public class MetricsConfiguration implements WebApplicationInitializer {
    final static NativeSigarLoader nativeSigarLoader = new OsAwareNativeSigarLoader();
    final static AtomicBoolean isMetricsEnabled = new AtomicBoolean(false);

    @Value("${graphite.server.host:graphite.service}")
    String graphiteHost;

    @Value("${graphite.server.port:2004}")
    int graphitePort;

    @Value("${graphite.server.pickled:true}")
    boolean usePickledGraphite;


    @Autowired
    MetricRegistry registry;

    public static boolean isMetricsEnabled() {
        return isMetricsEnabled.get();
    }

    @Bean
    public NamespaceResolver namespaceResolver() {
        log.info("Registering NamespaceResolver");
        return new UsernameBasedNamespaceResolver();
    }

    @Bean
    public MetricRegistry metricRegistry() {
        log.info("Registering MetricRegistry");
        return new MetricRegistry();
    }

    @Bean
    public Metrics codahaleMetricsFacade() {
        log.info("Registering Metrics facade based on Codahale");
        return new CodahaleMetricsFacade(metricRegistry());
    }

    @Bean
    public Filter codahaleMetricsFilter() {
        return new CodahaleMetricsFilter(metricRegistry());
    }

    @Bean
    public ScheduledReporter graphiteReporter(NamespaceResolver namespaceResolver) {
        ServerAddress server = new ServerAddress(graphiteHost, graphitePort);
        GraphiteSender sender = usePickledGraphite ?
                new PickledGraphite(server.inetSocketAddress()) :
                new GraphiteUDP(server.inetSocketAddress());
        ScheduledReporter reporter = createGraphiteReporter(registry, namespaceResolver, sender);
//        reporter.start(10, SECONDS);
        return reporter;
    }


    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        if (registry == null) {
            log.warn("Could not initialize Metrics.");
            return;
        }
        log.info("Enabling [{}] metrics collection to {}:{}", usePickledGraphite ? "pickled" : "regular", graphiteHost, graphitePort);

        isMetricsEnabled.set(true);

        /**
         * Keep track of the amount of Logs grouped by log level
         */
        try {
            log.info("Registering MetricsLogbackAppender");
            MetricsLogbackAppender logbackInstrumentedAppender = new MetricsLogbackAppender(registry);
            logbackInstrumentedAppender.start();

            Logger root = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
            root.addAppender(logbackInstrumentedAppender);
        } catch (Exception e) {
            log.warn("Could not initialize Log4j metrics. Counts per log-level will not be generated", e);
        }

        /**
         * Register a whole batch of Sigar related metrics.
         */
        try {
            nativeSigarLoader.loadNativeLibrary();

            log.info("Registering Sigar library to start collecting Metrics");
            SigarMetrics sigarMetrics = new SigarMetrics();
            sigarMetrics.registerGauges(registry);
        } catch (Throwable t) {
            log.warn("Could not initialize Sigar. System level metrics will not be generated", t);
        }

        registry.registerAll(new BufferPoolMetricSet());
        registry.registerAll(new ClassLoadingGaugeSet());
        registry.registerAll(new GarbageCollectorMetricSet());
        registry.registerAll(new MemoryUsageGaugeSet());
        registry.registerAll(new ThreadStatesGaugeSet());

        FileDescriptorRatioGauge gauge = new FileDescriptorRatioGauge();
        registry.register(gauge.metricsKey(), gauge);
    }

    protected ScheduledReporter createGraphiteReporter(MetricRegistry metricsRegistry, NamespaceResolver namespaceResolver, GraphiteSender sender) {
        return GraphiteReporter.forRegistry(metricsRegistry)
//                .prefixedWith(namespaceResolver.namespace())
                .convertRatesTo(SECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .filter(MetricFilter.ALL)
                .build(sender);
    }

    protected ScheduledReporter createStatsdReporter(MetricRegistry metricsRegistry, ServerAddress server, NamespaceResolver namespaceResolver) {
        Statsd statsd = new Statsd(server);
        return StatsdReporter.forRegistry(metricsRegistry)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .convertRatesTo(SECONDS)
                .filter(MetricFilter.ALL)
                .build(statsd);
    }
}
