package tix.metrics;

/**
 * Defines the full key for storing specific metrics using {@link Metrics}.
 *
 * @see tix.metrics.MetricsKeyBuilder For rules on how to make MetricsKeys
 */
public interface MetricsKey {
    final static String DEFAULT_SEPARATOR = ".";
    final static String DEFAULT_ERROR_SUFFIX = "ERROR";

    /**
     * Typically looks like:
     * WhateverService.saveSomething
     */
    String success();

    /**
     * In case a specific call threw an exception, we want to store
     * it under a specific error key. Typically simply appends {@link #DEFAULT_ERROR_SUFFIX}
     * to the success() key, but it depends on implementation of course.
     */
    String error();
}
