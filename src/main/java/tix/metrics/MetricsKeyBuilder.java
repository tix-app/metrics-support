package tix.metrics;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import tix.metrics.namespace.NamespaceResolver;

import java.util.Collection;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static org.springframework.util.StringUtils.collectionToDelimitedString;

/**
 * Based on:
 * http://matt.aimonetti.net/posts/2013/06/26/practical-guide-to-graphite-monitoring/
 *
 * Creates a namespace according to:
 * <namespace>.<instrumented section>.<target (noun)>.<action (past tense verb)>
 *
 * For example:
 * accounts.authentication.password.attempted
 * accounts.authentication.password.succeeded
 * accounts.authentication.password.failed
 */
@Slf4j
@Configurable(autowire = Autowire.BY_NAME, preConstruction = true)
public class MetricsKeyBuilder {
    @Autowired
    NamespaceResolver namespaceResolver;

    private String instrumentedSection;
    private String target;
    private String action;

    public static MetricsKeyBuilder create() {
        return new MetricsKeyBuilder();
    }

    public MetricsKeyBuilder withInstrumentedSection(String instrumentedSection) {
        this.instrumentedSection = instrumentedSection;
        return this;
    }

    public MetricsKeyBuilder withTarget(String noun) {
        this.target = noun;
        return this;
    }

    public MetricsKeyBuilder withAction(String pastTenseVerb) {
        this.action = pastTenseVerb;
        return this;
    }

    public MetricsKeyBuilder copy() {
        return new MetricsKeyBuilder()
                .withInstrumentedSection(instrumentedSection)
                .withTarget(target)
                .withAction(action);
    }

    public MetricsKey buildWithAction(String pastTenseVerb) {
        return withAction(pastTenseVerb).build();
    }

    public MetricsKey buildWithAction(Collection pastTenseVerbs) {
        String concatenatedVerb = collectionToDelimitedString(pastTenseVerbs, MetricsKey.DEFAULT_SEPARATOR);
        return withAction(concatenatedVerb).build();
    }

    public MetricsKey build() {
        return new DefaultMetricsKey(namespaceResolver.namespace(), instrumentedSection, target, action);
    }

    protected class DefaultMetricsKey implements MetricsKey {
        List<String> tokens = newArrayList();

        public DefaultMetricsKey(String namespace, String instrumentedSection, String target, String action) {
            safeAdd(namespace);
            safeAdd(instrumentedSection);
            safeAdd(target);
            safeAdd(action);
        }

        @Override
        public String success() {
            return collectionToDelimitedString(tokens, DEFAULT_SEPARATOR);
        }

        @Override
        public String error() {
            return success() + DEFAULT_SEPARATOR + DEFAULT_ERROR_SUFFIX;
        }

        private void safeAdd(String target) {
            if (target != null) {
                tokens.add(target);
            }
        }
    }
}
