package tix.metrics;

import tix.metrics.time.Duration;
import tix.metrics.time.Interval;

import java.util.concurrent.Callable;

/**
 * Metrics implementation that does nothing.
 */
public class NoopMetrics implements Metrics {
    @Override
    public Metrics count(MetricsKey aspect, int delta) {
        return this;
    }

    @Override
    public Metrics inc(MetricsKey key) {
        return this;
    }

    @Override
    public Metrics dec(MetricsKey key) {
        return this;
    }

    @Override
    public <V> Metrics gauge(MetricsKey key, Interval interval, V value) {
        return this;
    }

    @Override
    public <V> Metrics gauge(MetricsKey key, Interval interval, Callable<V> function) {
        return this;
    }

    @Override
    public Metrics time(MetricsKey key, Duration duration) {
        return this;
    }

    @Override
    public Metrics histogram(MetricsKey key, int value) {
        return this;
    }

    @Override
    public Metrics meter(MetricsKey key) {
        return this;
    }

    @Override
    public Metrics error(MetricsKey metricsKey) {
        return this;
    }

    @Override
    public void destroy() throws Exception {
    }
}
