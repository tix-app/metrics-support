package tix.metrics;

import org.apache.commons.lang3.SystemUtils;

public class OperatingSystem {
    public static String name() {
        return SystemUtils.OS_NAME;
    }

    public static boolean isWindows() {
        return SystemUtils.IS_OS_WINDOWS;
    }
    public static boolean isOsx() {
        return SystemUtils.IS_OS_MAC_OSX;
    }
    public static boolean isLinux() {
        return SystemUtils.IS_OS_LINUX;
    }

    public static boolean isAmd() {
        return System.getProperty("os.arch").contains("amd");
    }

    public static boolean isX86() {
        return System.getProperty("os.arch").endsWith("86");
    }
}
