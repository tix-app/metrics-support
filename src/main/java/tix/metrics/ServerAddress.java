package tix.metrics;

import java.net.InetSocketAddress;

public class ServerAddress {
    public final String host;
    public final int port;

    public ServerAddress(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public InetSocketAddress inetSocketAddress() {
        return new InetSocketAddress(host, port);
    }

    public String host() {
        return host;
    }

    public int port() {
        return port;
    }
}
