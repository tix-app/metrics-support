package tix.metrics.annotation;

import org.springframework.beans.factory.annotation.Autowired;
import tix.metrics.Metrics;
import tix.metrics.NoopMetrics;

/**
 * Abstract class that allows the Metrics to be set. If not, a default implementation is returned.
 */
public abstract class AbstractAspect {

    private Metrics metrics;

    protected Metrics metrics() {
        if (metrics == null) {
            metrics = new NoopMetrics();
        }
        return metrics;
    }

    @Autowired(required = false)
    public void setMetrics(Metrics metrics) {
        this.metrics = metrics;
    }
}
