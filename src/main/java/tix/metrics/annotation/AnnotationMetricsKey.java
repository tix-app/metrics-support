package tix.metrics.annotation;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import tix.metrics.MetricsKey;
import tix.metrics.MetricsKeyBuilder;

import java.lang.reflect.Method;

public class AnnotationMetricsKey implements MetricsKey {
    private final MetricsKeyBuilder builder;

    public static AnnotationMetricsKey from(ProceedingJoinPoint pjp) {
        return new AnnotationMetricsKey(pjp);
    }

    protected AnnotationMetricsKey(ProceedingJoinPoint pjp) {
        Method method = toMethod(pjp);
        builder = MetricsKeyBuilder
                .create()
                .withInstrumentedSection("annotation")
                .withTarget(method.getDeclaringClass().getSimpleName())
                .withAction(method.getName());
    }

    @Override
    public String success() {
        return builder.build().success();
    }

    @Override
    public String error() {
        return builder.build().error();
    }


    private static Method toMethod(ProceedingJoinPoint pjp) {
        MethodSignature methodSignature = (MethodSignature) pjp.getSignature();
        return methodSignature.getMethod();
    }
}
