package tix.metrics.annotation;

import lombok.NoArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import tix.metrics.Metrics;
import tix.metrics.MetricsConfiguration;
import tix.metrics.MetricsKey;
import tix.metrics.NoopMetrics;
import tix.metrics.time.Interval;

/**
 * This class declares the aspects for the system.  In this example is an around advice for periodically
 * sending a Gauge value to the Metrics server.
 *
 * In order to have Aspects working correctly, please configure AspectJ's Load-Time Weaving mechanism by
 * adding a JVM javaagent param:
 * <code>
 *   -javaagent:/Users/erikwiersma/.m2/repository/org/springframework/spring-instrument/3.2.0.RELEASE/spring-instrument-3.2.0.RELEASE.jar
 * </code>
 */
@Aspect
@NoArgsConstructor
@Configurable(autowire = Autowire.BY_NAME, dependencyCheck = true, preConstruction = true)
public class CachedGaugeAspect extends AbstractAspect {
    private static final Logger LOG = LoggerFactory.getLogger(CountedAspect.class);

    /**
     * This around advice adds timing to any method annotated with the CachedGauge annotation.
     * It binds the annotation to the parameter CachedGauge so that the values are available at runtime.
     * Also note that the retention policy of the annotation needs to be RUNTIME.
     *
     * @param pjp the join point for this advice
     * @param annotation the CachedGauge annotation as declared on the method
     */
    @Around("@annotation(annotation)")
    public Object processSystemRequest(final ProceedingJoinPoint pjp, CachedGauge annotation) throws Throwable {
        Object retVal = pjp.proceed();
        safeMeasurement(pjp, annotation, retVal);
        return retVal;
    }

    protected void safeMeasurement(ProceedingJoinPoint pjp, CachedGauge annotation, Object value) {
        if (!MetricsConfiguration.isMetricsEnabled()) {
            return;
        }

        try {
            MetricsKey key = AnnotationMetricsKey.from(pjp);
            Interval interval = Interval.from(annotation.timeout(), annotation.timeoutUnit());
            metrics().gauge(key, interval, value);
        } catch (RuntimeException e) {
            LOG.info("Aspect could not measure performance", e);
        }
    }
}