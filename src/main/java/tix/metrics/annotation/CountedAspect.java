package tix.metrics.annotation;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import tix.metrics.Metrics;
import tix.metrics.MetricsConfiguration;
import tix.metrics.NoopMetrics;

import static tix.metrics.annotation.AnnotationMetricsKey.from;

/**
 * This class declares the aspects for the system.  In this example is an around advice for periodically
 * sending a Gauge value to the Metrics server.
 */
@Aspect
@Configurable(autowire = Autowire.BY_NAME, dependencyCheck = true, preConstruction = true)
public class CountedAspect extends AbstractAspect {
    private static final Logger LOG = LoggerFactory.getLogger(CountedAspect.class);

    public CountedAspect() {
    }

    /**
     * This around advice adds timing to any method annotated with the Timed annotation.
     * It binds the annotation to the parameter timedAnnotation so that the values are available at runtime.
     * Also note that the retention policy of the annotation needs to be RUNTIME.
     *
     * @param pjp the join point for this advice
     * @param annotation the CachedGauge annotation as declared on the method
     */
    @Around("@annotation(annotation)")
    public Object processSystemRequest(final ProceedingJoinPoint pjp, Counted annotation) throws Throwable {
        Object retVal = pjp.proceed();
        safeMeasurement(pjp);
        return retVal;
    }

    protected void safeMeasurement(ProceedingJoinPoint pjp) {
        if (!MetricsConfiguration.isMetricsEnabled()) {
            return;
        }

        try {
            metrics().meter(from(pjp));
        } catch (RuntimeException e) {
            LOG.info("Aspect could not measure performance", e);
        }
    }

}