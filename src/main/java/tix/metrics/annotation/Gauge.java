package tix.metrics.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * An annotation for marking a method as a gauge
 *
 * <p/>
 * Given a method like this:
 * <pre><code>
 *     {@literal @}Gauge()
 *     public int getQueueSize() {
 *         return queue.getSize();
 *     }
 * 
 * </code></pre>
 * <p/>
 * 
 * A gauge for the defining class with the name queueSize will be created which uses the annotated method's
 * return value as its value
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Gauge {
}
