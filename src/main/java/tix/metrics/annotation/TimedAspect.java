package tix.metrics.annotation;

import com.google.common.base.Stopwatch;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import tix.metrics.*;
import tix.metrics.time.Duration;

/**
 * This class declares the aspects for the system.  In this example is an around advice for timing the length of methods
 * marked with the Timed annotation.
 *
 * In order to have Aspects working correctly, please configure AspectJ's Load-Time Weaving mechanism by
 * adding a JVM javaagent param:
 * <code>
 *   -javaagent:/Users/erikwiersma/.m2/repository/org/springframework/spring-instrument/4.1.0.RELEASE/spring-instrument-3.2.0.RELEASE.jar
 * </code>
 */
@Aspect
@Slf4j
@Configurable(autowire = Autowire.BY_NAME, dependencyCheck = false, preConstruction = true)
public class TimedAspect extends AbstractAspect {

    public TimedAspect() {
    }

    /**
     * This around advice adds timing to any method annotated with the Timed annotation.
     * It binds the annotation to the parameter timedAnnotation so that the values are available at runtime.
     * Also note that the retention policy of the annotation needs to be RUNTIME.
     *
     * @param pjp the join point for this advice
     * @param timedAnnotation the Timed annotation as declared on the method
     */
    @Around("@annotation(timedAnnotation)")
    public Object processSystemRequest(final ProceedingJoinPoint pjp, Timed timedAnnotation) throws Throwable {
        try {
            Stopwatch stopwatch = Stopwatch.createStarted();
            Object retVal = pjp.proceed();
            stopwatch.stop();

            safeMeasurement(pjp, stopwatch);

            return retVal;
        } catch (RuntimeException runtimeException) {
            metrics().error(new ExceptionMetricsKey(runtimeException));
            throw runtimeException;
        }
    }

    protected void safeMeasurement(ProceedingJoinPoint pjp, Stopwatch stopwatch) {
        if (!MetricsConfiguration.isMetricsEnabled()) {
            return;
        }

        try {
            MetricsKey key = AnnotationMetricsKey.from(pjp);
            Duration duration = Duration.from(stopwatch);
            metrics().time(key, duration);
        } catch (RuntimeException e) {
            log.info("Aspect could not measure performance", e);
        }
    }
}