/**
 * <p/>
 * Works closely together with {@link nl.chess.it.Metrics}. This
 * package contains Annotations with corresponding Aspects.
 * <p/>
 * In order to have Aspects working correctly, please configure AspectJ's Load-Time Weaving mechanism by
 * adding a JVM javaagent param:
 * <code>
 *   -javaagent:/Users/{username}/.m2/repository/org/springframework/spring-instrument/3.2.0.RELEASE/spring-instrument-3.2.0.RELEASE.jar
 * </code>
 * <p/>
 * See also:
 * <a href="http://docs.spring.io/spring/docs/3.0.0.M4/reference/html/ch07s08.html#aop-aj-ltw">Explanation on AOP AspectJ Load-time-weaving (LTW)</a>
 * <a href="http://blog.espenberntsen.net/2010/03/20/aspectj-cheat-sheet/">Cheat sheet for making AspectJ pointcuts</a>
 */
package tix.metrics.annotation;