package tix.metrics.codahale;

import com.codahale.metrics.CachedGauge;
import com.codahale.metrics.MetricRegistry;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tix.metrics.Metrics;
import tix.metrics.MetricsKey;
import tix.metrics.time.Duration;
import tix.metrics.time.Interval;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 *
 * Implements T2M's generic Metrics interface. Delegates all
 * calls to Yammer's MetricRegistry (Codahale).
 *
 * See package-info.java for more info.
 */
@Slf4j
public class CodahaleMetricsFacade implements Metrics {
    private final MetricRegistry metricsRegistry;

    public CodahaleMetricsFacade(MetricRegistry metricsRegistry) {
        this.metricsRegistry = metricsRegistry;
    }

    @Override
    public Metrics histogram(MetricsKey key, int value) {
        logKey("histogram", key);
        metricsRegistry.histogram(key.success()).update(value);
        return this;
    }

    @Override
    public Metrics meter(MetricsKey key) {
        if (key == null) {
            return this;
        }
        logKey("meter", key);
        metricsRegistry.meter(key.success()).mark();
        return this;
    }

    @Override
    public Metrics count(MetricsKey key, int delta) {
        if (key == null) {
            return this;
        }
        logKey("count", key);
        metricsRegistry.counter(key.success()).inc(delta);
        return this;
    }

    @Override
    public Metrics inc(MetricsKey key) {
        return count(key, 1);
    }

    @Override
    public Metrics dec(MetricsKey key) {
        return count(key, -1);
    }

    @Override
    public Metrics time(MetricsKey key, Duration duration) {
        if (key == null) {
            return this;
        }
        logKey("time", key);
        metricsRegistry.timer(key.success()).update(duration.toMillis(), TimeUnit.MILLISECONDS);
        return this;
    }

    private void logKey(String type, MetricsKey key) {
        if (log.isTraceEnabled()) {
            log.trace("Sending [{}] metrics to [{}]", type, key.success());
        }
    }

    @Override
    public <V> Metrics gauge(final MetricsKey key, Interval interval, final V value) {
        if (key == null) {
            return this;
        }
        if (value != null) {
            metricsRegistry.register(key.success(), new CachedGauge<V>(interval.toMillis(), TimeUnit.MILLISECONDS) {
                @Override
                protected V loadValue() {
                    logKey("gauge", key);
                    return value;
                }
            });
        }
        return this;
    }

    @Override
    public <V> Metrics gauge(final MetricsKey key, Interval interval, final Callable<V> function) {
        if (key == null) {
            return this;
        }
        metricsRegistry.register(key.success(), new CachedGauge<V>(interval.toMillis(), TimeUnit.MILLISECONDS) {
            @Override
            protected V loadValue() {
                try {
                    logKey("gauge", key);
                    return function.call();
                } catch (Exception e) {
                    log.info("Could not gauge key [" + key.success() + "]", e);
                    return null;
                }
            }
        });
        return this;
    }

    @Override
    public Metrics error(MetricsKey metricsKey) {
        if (metricsKey == null) {
            return this;
        }
        metricsRegistry.counter(metricsKey.error()).inc();
        return this;
    }

    @Override
    public void destroy() throws Exception {
    }
}
