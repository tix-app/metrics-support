package tix.metrics.codahale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Arrays;

import static org.apache.commons.lang3.ArrayUtils.indexOf;
import static org.springframework.util.StringUtils.arrayToCommaDelimitedString;

public class Frame {
    private static final Logger LOG = LoggerFactory.getLogger(Frame.class);

    public static final byte NEWLINE_BYTE = (byte) '\n';

    private final Mtu mtu;
    private final ByteArrayOutputStream outputData;

    private Writer writer;

    private int size = 0;
    private int cursor = 0;

    public Frame(Mtu mtu) {
        this.mtu = mtu;
        this.outputData = new ByteArrayOutputStream();
        reset();
    }

    public Frame appendConditionally(boolean condition, String... tokens) {
        if (condition) {
            return append(tokens);
        }
        return this;
    }

    public Frame append(String... tokens) {
        try {
            for (String token : tokens) {
                writer.write(token);
                size += token.length();
            }
            writer.flush();
        } catch (IOException e) {
            LOG.warn("Error sending [" + arrayToCommaDelimitedString(tokens) + "] to Statsd", e);
        }
        return this;
    }

    public Frame reset() {
        outputData.reset();
        writer = new BufferedWriter(new OutputStreamWriter(outputData));
        size = 0;
        cursor = 0;

        return this;
    }

    public boolean hasMorePackets() {
        return cursor < size;
    }


    public byte[] nextPacket() {
        byte[] output = outputData.toByteArray();
        int windowSize = mtu.suggestedCutoff();

        int estimatedNextCutoff = cursor + windowSize;
        int firstNewlineAfterCutoff = indexOf(output, NEWLINE_BYTE, estimatedNextCutoff) + 1;
        firstNewlineAfterCutoff = preventOutOfBounds(output, firstNewlineAfterCutoff);

        /*
         * Handle the last part of the output data. If there is no more new lines
         * found, but there are still bytes available, simply read the remaining bytes.
         */
        if (firstNewlineAfterCutoff == 0 && hasMorePackets()) {
            firstNewlineAfterCutoff = output.length;
        }

        byte[] window = Arrays.copyOfRange(output, cursor, firstNewlineAfterCutoff);
        cursor = firstNewlineAfterCutoff;
        return window;
    }

    protected int preventOutOfBounds(byte[] output, int estimatedNextCutoff) {
        if (estimatedNextCutoff > output.length) {
            return output.length;
        }
        return estimatedNextCutoff;
    }
}
