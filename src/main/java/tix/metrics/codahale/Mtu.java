package tix.metrics.codahale;

/**
 * Packet sizes are important when using UDP. Especially in case of Statsd where
 * too large packets are simply discarded.
 * <p/>
 * Read also here:          http://serverfault.com/questions/325982/is-there-such-a-thing-as-a-typical-or-de-facto-default-for-mtu-size
 * or more specifically:    http://serverfault.com/a/325987
 * <p/>
 * <blockquote>
 * Is 1500 common? Yes, absolutely, for traditional ethernet on a LAN.
 * Is it safe to assume 1500 or expect it for any connection? No. Mobile users that connect from anywhere may have different MTU's at different times.
 * </blockquote>
 */
public class Mtu {
    private static final Mtu STATSD_MTU = new Mtu(1500, 1024);

    private final int maxSize;
    private final int suggestedCutoff;

    private Mtu(int maxSize, int suggestedCutoff) {
        this.maxSize = maxSize;
        this.suggestedCutoff = suggestedCutoff;
    }

    public static Mtu statsdMtu() {
        return STATSD_MTU;
    }

    public static Mtu customMtu(int maxSize, int suggestedCutoff) {
        return new Mtu(maxSize, suggestedCutoff);
    }

    public int maxSize() {
        return maxSize;
    }

    public int suggestedCutoff() {
        return suggestedCutoff;
    }
}
