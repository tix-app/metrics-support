package tix.metrics.codahale;

public enum StatType {
    COUNTER("c"),
    TIMER("ms"),
    GAUGE("g");

    /**
     * The value of this Stat as it is being sent on the wire
     */
    private final String wireValue;

    StatType(String wireValue) {
        this.wireValue = wireValue;
    }

    public String wireValue() {
        return wireValue;
    }

    public static StatType fromWireValue(String wireValue) {
        for (StatType statType : values()) {
            if (statType.wireValue.equals(wireValue)) {
                return statType;
            }
        }
        throw new IllegalStateException("Could not convert " + wireValue + " to any StatType");
    }
}
