package tix.metrics.codahale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tix.metrics.MetricsException;
import tix.metrics.ServerAddress;

import java.io.Closeable;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.regex.Pattern;

import static tix.metrics.codahale.Mtu.statsdMtu;

/**
 * A client to a StatsD server.
 *
 * Not thread-safe
 */
public class Statsd implements Closeable {
    private static final Logger LOG = LoggerFactory.getLogger(Statsd.class);

    private static final Pattern WHITESPACE = Pattern.compile("[\\s]+");

    private final InetAddress host;
    private final int port;
    private final ThreadLocal<Frame> currentPacket = new ThreadLocal<Frame>();

    private DatagramSocket datagramSocket;

    private boolean prependNewline = false;

    public Statsd(ServerAddress serverAddress) {
        this.host = createInetAddress(serverAddress.host());
        this.port = serverAddress.port();
    }

    private Frame resolveCurrentFrame() {
        if (currentPacket.get() == null) {
            Frame frame = new Frame(statsdMtu());
            currentPacket.set(frame);
        }
        return currentPacket.get();
    }

    public void send(String name, String value, StatType statType) throws IOException {
        String sanitizedName = sanitizeString(name);
        resolveCurrentFrame()
                .appendConditionally(prependNewline, "\n")
                .append(sanitizedName, ":", value, "|", statType.wireValue());

        prependNewline = true;
    }

    @Override
    public void close() throws IOException {
        try {
            Frame frame = resolveCurrentFrame();
            while (frame.hasMorePackets()) {
                byte[] packet = frame.nextPacket();
                doSendAndClose(packet);
            }

            frame.reset();
        } finally {
            currentPacket.set(null);
        }

    }

    protected void doSendAndClose(byte[] bytes) throws IOException {
        try {
            datagramSocket = new DatagramSocket();

            if (LOG.isTraceEnabled()) {
                LOG.trace("Writing to socket {}: [\n{}\n]", datagramSocket, new String(bytes));
            }

            DatagramPacket datagramPacket = new DatagramPacket(bytes, bytes.length, host, port);
            datagramSocket.send(datagramPacket);
        } finally {
            if (datagramSocket != null) {
                datagramSocket.close();
                datagramSocket = null;
            }
        }
    }

    private String sanitizeString(String s) {
        return WHITESPACE.matcher(s).replaceAll("-");
    }

    protected InetAddress createInetAddress(String host) {
        try {
            return InetAddress.getByName(host);
        } catch (UnknownHostException e) {
            throw new MetricsException("Could not parse host [" + host + "]", e);
        }
    }
}