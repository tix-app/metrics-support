package tix.metrics.codahale;

import com.codahale.metrics.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Locale;
import java.util.Map;
import java.util.SortedMap;
import java.util.concurrent.TimeUnit;

import static tix.metrics.codahale.StatType.GAUGE;
import static tix.metrics.codahale.StatType.TIMER;

/**
 * A reporter which publishes metric values to a Statds server.
 *
 * @see <a href="https://github.com/etsy/statsd">Statsd</a>
 */
public class StatsdReporter extends ScheduledReporter {
    /**
     * Returns a new {@link Builder} for {@link StatsdReporter}.
     *
     * @param registry the registry to report
     * @return a {@link Builder} instance for a {@link StatsdReporter}
     */
    public static Builder forRegistry(MetricRegistry registry) {
        return new Builder(registry);
    }

    /**
     * A builder for {@link StatsdReporter} instances. Defaults to not using a metricsKey, using the
     * default clock, converting rates to events/second, converting durations to milliseconds, and
     * not filtering metrics.
     */
    public static class Builder {
        private final MetricRegistry registry;
        private TimeUnit rateUnit;
        private TimeUnit durationUnit;
        private MetricFilter filter;

        private Builder(MetricRegistry registry) {
            this.registry = registry;
            this.rateUnit = TimeUnit.SECONDS;
            this.durationUnit = TimeUnit.MILLISECONDS;
            this.filter = MetricFilter.ALL;
        }

        /**
         * Convert rates to the given time unit.
         *
         * @param rateUnit a unit of time
         * @return {@code this}
         */
        public Builder convertRatesTo(TimeUnit rateUnit) {
            this.rateUnit = rateUnit;
            return this;
        }

        /**
         * Convert durations to the given time unit.
         *
         * @param durationUnit a unit of time
         * @return {@code this}
         */
        public Builder convertDurationsTo(TimeUnit durationUnit) {
            this.durationUnit = durationUnit;
            return this;
        }

        /**
         * Only report metrics which match the given filter.
         *
         * @param filter a {@link com.codahale.metrics.MetricFilter}
         * @return {@code this}
         */
        public Builder filter(MetricFilter filter) {
            this.filter = filter;
            return this;
        }

        /**
         * Builds a {@link StatsdReporter} with the given properties, sending metrics using the
         * given {@link tix.metrics.codahale.Statsd} client.
         *
         * @param statsd a {@link tix.metrics.codahale.Statsd} client
         * @return a {@link StatsdReporter}
         */
        public StatsdReporter build(Statsd statsd) {
            return new StatsdReporter(registry,
                    statsd,
                    filter,
                    rateUnit,
                    durationUnit);
        }
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(StatsdReporter.class);

    private final Statsd statsd;

    public StatsdReporter(MetricRegistry registry,
                          Statsd statsd,
                          MetricFilter filter,
                          TimeUnit rateUnit,
                          TimeUnit durationUnit) {
        super(registry, "statsd-reporter", filter, rateUnit, durationUnit);

        this.statsd = statsd;
    }

    @Override
    public void report(SortedMap<String, Gauge> gauges,
                       SortedMap<String, Counter> counters,
                       SortedMap<String, Histogram> histograms,
                       SortedMap<String, Meter> meters,
                       SortedMap<String, Timer> timers) {

        try {
            for (Map.Entry<String, Gauge> entry : gauges.entrySet()) {
                reportGauge(entry.getKey(), entry.getValue());
            }

            for (Map.Entry<String, Counter> entry : counters.entrySet()) {
                reportCounter(entry.getKey(), entry.getValue());
            }

            for (Map.Entry<String, Histogram> entry : histograms.entrySet()) {
                reportHistogram(entry.getKey(), entry.getValue());
            }

            for (Map.Entry<String, Meter> entry : meters.entrySet()) {
                reportMetered(entry.getKey(), entry.getValue());
            }

            for (Map.Entry<String, Timer> entry : timers.entrySet()) {
                reportTimer(entry.getKey(), entry.getValue());
            }

        } catch(IOException e) {
            LOGGER.warn("Unable to report to StatsD", statsd, e);
        } finally {
            try {
                statsd.close();
            } catch (IOException e) {
                LOGGER.debug("Error disconnecting from StatsD server", statsd, e);
            }
        }
    }

    private void reportTimer(String name, Timer timer) throws IOException {
        final Snapshot snapshot = timer.getSnapshot();

        statsd.send(name + ".max", format(convertDuration(snapshot.getMax())), TIMER);
        statsd.send(name + ".mean", format(convertDuration(snapshot.getMean())), TIMER);
        statsd.send(name + ".min", format(convertDuration(snapshot.getMin())), TIMER);
        statsd.send(name + ".stddev", format(convertDuration(snapshot.getStdDev())), TIMER);
        statsd.send(name + ".p50", format(convertDuration(snapshot.getMedian())), TIMER);
        statsd.send(name + ".p75", format(convertDuration(snapshot.get75thPercentile())), TIMER);
        statsd.send(name + ".p95", format(convertDuration(snapshot.get95thPercentile())), TIMER);
        statsd.send(name + ".p98", format(convertDuration(snapshot.get98thPercentile())), TIMER);
        statsd.send(name + ".p99", format(convertDuration(snapshot.get99thPercentile())), TIMER);
        statsd.send(name + ".p999", format(convertDuration(snapshot.get999thPercentile())), TIMER);

        reportMetered(name, timer);
    }

    private void reportMetered(String name, Metered meter) throws IOException {
        statsd.send(name + ".count", format(meter.getCount()), StatType.GAUGE);
        statsd.send(name + ".m1_rate", format(convertRate(meter.getOneMinuteRate())), TIMER);
        statsd.send(name + ".m5_rate", format(convertRate(meter.getFiveMinuteRate())), TIMER);
        statsd.send(name + ".m15_rate", format(convertRate(meter.getFifteenMinuteRate())), TIMER);
        statsd.send(name + ".mean_rate", format(convertRate(meter.getMeanRate())), TIMER);
    }

    private void reportHistogram(String name, Histogram histogram) throws IOException {
        final Snapshot snapshot = histogram.getSnapshot();
        statsd.send(name + ".count", format(histogram.getCount()), GAUGE);
        statsd.send(name + ".max", format(snapshot.getMax()), TIMER);
        statsd.send(name + ".mean", format(snapshot.getMean()), TIMER);
        statsd.send(name + ".min", format(snapshot.getMin()), TIMER);
        statsd.send(name + ".stddev", format(snapshot.getStdDev()), TIMER);
        statsd.send(name + ".p50", format(snapshot.getMedian()), TIMER);
        statsd.send(name + ".p75", format(snapshot.get75thPercentile()), TIMER);
        statsd.send(name + ".p95", format(snapshot.get95thPercentile()), TIMER);
        statsd.send(name + ".p98", format(snapshot.get98thPercentile()), TIMER);
        statsd.send(name + ".p99", format(snapshot.get99thPercentile()), TIMER);
        statsd.send(name + ".p999", format(snapshot.get999thPercentile()), TIMER);
    }

    private void reportCounter(String name, Counter counter) throws IOException {
        statsd.send(name + ".count", format(counter.getCount()), StatType.COUNTER);
    }

    private void reportGauge(String name, Gauge gauge) throws IOException {
        final String value = format(gauge.getValue());
        if (value != null) {
            statsd.send(name, value, StatType.GAUGE);
        }
    }

    private String format(Object o) {
        if (o instanceof Float) {
            return format(((Float) o).doubleValue());
        } else if (o instanceof Double) {
            return format(((Double) o).doubleValue());
        } else if (o instanceof Byte) {
            return format(((Byte) o).longValue());
        } else if (o instanceof Short) {
            return format(((Short) o).longValue());
        } else if (o instanceof Integer) {
            return format(((Integer) o).longValue());
        } else if (o instanceof Long) {
            return format(((Long) o).longValue());
        }
        return null;
    }

    private String format(long n) {
        return Long.toString(n);
    }

    private String format(double v) {
        return String.format(Locale.US, "%2.2f", v);
    }
}