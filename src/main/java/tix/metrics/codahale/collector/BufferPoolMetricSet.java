package tix.metrics.codahale.collector;

import com.codahale.metrics.JmxAttributeGauge;
import com.codahale.metrics.Metric;
import com.codahale.metrics.MetricSet;
import lombok.extern.slf4j.Slf4j;
import tix.metrics.MetricsKeyBuilder;

import javax.management.JMException;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.google.common.collect.Lists.newArrayList;
import static java.lang.management.ManagementFactory.getPlatformMBeanServer;

/**
 * A set of gauges for the count, usage, and capacity of the JVM's direct and mapped buffer pools.
 * <p>
 * These JMX objects are only available on Java 7 and above.
 */
@Slf4j
public class BufferPoolMetricSet implements MetricSet {
    private final MetricsKeyBuilder metricsKeyBuilder = MetricsKeyBuilder
            .create()
            .withInstrumentedSection("jvm")
            .withTarget("bufferpool");

    private static final String[] ATTRIBUTES = { "Count", "MemoryUsed", "TotalCapacity" };
    private static final String[] NAMES = { "count", "used", "capacity" };
    private static final String[] POOLS = { "direct", "mapped" };

    private final MBeanServer mBeanServer;

    public BufferPoolMetricSet() {
        this(getPlatformMBeanServer());
    }

    public BufferPoolMetricSet(MBeanServer mBeanServer) {
        this.mBeanServer = mBeanServer;
    }

    private String metricsKey(String action, String subAction) {
        return metricsKeyBuilder.buildWithAction(newArrayList(action, subAction)).success();
    }

    @Override
    public Map<String, Metric> getMetrics() {
        final Map<String, Metric> gauges = new HashMap<>();
        for (String pool : POOLS) {
            for (int i = 0; i < ATTRIBUTES.length; i++) {
                final String attribute = ATTRIBUTES[i];
                final String name = NAMES[i];
                try {
                    final ObjectName on = new ObjectName("java.nio:type=BufferPool,name=" + pool);
                    mBeanServer.getMBeanInfo(on);
                    gauges.put(metricsKey(pool, name),
                            new JmxAttributeGauge(mBeanServer, on, attribute));
                } catch (JMException ex) {
                    log.debug("Unable to load buffer pool MBeans, possibly running on Java 6", ex);
                }
            }
        }
        return Collections.unmodifiableMap(gauges);
    }
}