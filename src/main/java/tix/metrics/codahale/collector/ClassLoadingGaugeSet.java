package tix.metrics.codahale.collector;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.Metric;
import com.codahale.metrics.MetricSet;
import tix.metrics.MetricsKeyBuilder;

import java.lang.management.ClassLoadingMXBean;
import java.lang.management.ManagementFactory;
import java.util.HashMap;
import java.util.Map;

/**
 * A set of gauges for JVM classloader usage.
 */
public class ClassLoadingGaugeSet implements MetricSet {
    private final MetricsKeyBuilder metricsKeyBuilder = MetricsKeyBuilder
            .create()
            .withInstrumentedSection("jvm")
            .withTarget("classloader");

    private final ClassLoadingMXBean mxBean;

    public ClassLoadingGaugeSet() {
        this(ManagementFactory.getClassLoadingMXBean());
    }

    public ClassLoadingGaugeSet(ClassLoadingMXBean mxBean) {
        this.mxBean = mxBean;
    }

    private String metricsKey(String action) {
        return metricsKeyBuilder.buildWithAction(action).success();
    }

    @Override
    public Map<String, Metric> getMetrics() {
        final Map<String, Metric> gauges = new HashMap<>();
        gauges.put(metricsKey("loaded"), (Gauge<Long>) mxBean::getTotalLoadedClassCount);
        gauges.put(metricsKey("unloaded"), (Gauge<Long>) mxBean::getUnloadedClassCount);
        return gauges;
    }
}