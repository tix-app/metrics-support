package tix.metrics.codahale.collector;

import com.codahale.metrics.RatioGauge;
import lombok.extern.slf4j.Slf4j;
import tix.metrics.MetricsKeyBuilder;

import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * A gauge for the ratio of used to total file descriptors.
 */
@Slf4j
public class FileDescriptorRatioGauge extends RatioGauge {
    private final MetricsKeyBuilder metricsKeyBuilder = MetricsKeyBuilder
            .create()
            .withInstrumentedSection("os")
            .withTarget("fs");

    private final OperatingSystemMXBean os;

    /**
     * Creates a new gauge using the platform OS bean.
     */
    public FileDescriptorRatioGauge() {
        this(ManagementFactory.getOperatingSystemMXBean());
    }

    /**
     * Creates a new gauge using the given OS bean.
     *
     * @param os    an {@link OperatingSystemMXBean}
     */
    public FileDescriptorRatioGauge(OperatingSystemMXBean os) {
        this.os = os;
    }

    public String metricsKey() {
        return metricsKeyBuilder.buildWithAction("openfiles").success();
    }

    @Override
    protected Ratio getRatio() {
        try {
            return Ratio.of(invoke("getOpenFileDescriptorCount"), invoke("getMaxFileDescriptorCount"));
        } catch (Exception e) {
            log.debug("Could not calculate file descriptor ratio", e);
            return Ratio.of(Double.NaN, Double.NaN);
        }
    }

    private long invoke(String name) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        final Method method = os.getClass().getDeclaredMethod(name);
        method.setAccessible(true);
        return (Long) method.invoke(os);
    }
}