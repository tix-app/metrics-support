package tix.metrics.codahale.collector;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.Metric;
import com.codahale.metrics.MetricSet;
import tix.metrics.MetricsKeyBuilder;

import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.util.*;
import java.util.regex.Pattern;

import static com.google.common.collect.Lists.newArrayList;

/**
 * A set of gauges for the counts and elapsed times of garbage collections.
 */
public class GarbageCollectorMetricSet implements MetricSet {
    private static final Pattern WHITESPACE = Pattern.compile("[\\s]+");

    private final MetricsKeyBuilder metricsKeyBuilder;
    private final List<GarbageCollectorMXBean> garbageCollectors;

    /**
     * Creates a new set of gauges for all discoverable garbage collectors.
     */
    public GarbageCollectorMetricSet() {
        this(ManagementFactory.getGarbageCollectorMXBeans());
    }

    /**
     * Creates a new set of gauges for the given collection of garbage collectors.
     *
     * @param garbageCollectors    the garbage collectors
     */
    public GarbageCollectorMetricSet(Collection<GarbageCollectorMXBean> garbageCollectors) {
        this.garbageCollectors = newArrayList(garbageCollectors);
        this.metricsKeyBuilder = MetricsKeyBuilder
                .create()
                .withInstrumentedSection("jvm")
                .withTarget("gc");
    }

    private String metricsKey(String action) {
        return metricsKeyBuilder.buildWithAction(action).success();
    }

    @Override
    public Map<String, Metric> getMetrics() {
        final Map<String, Metric> gauges = new HashMap<String, Metric>();
        for (final GarbageCollectorMXBean gc : garbageCollectors) {
            final String name = WHITESPACE.matcher(gc.getName()).replaceAll("-");
            gauges.put(metricsKey(name + ".count"), (Gauge<Long>) gc::getCollectionCount);

            gauges.put(metricsKey(name + ".time"), (Gauge<Long>) gc::getCollectionTime);
        }
        return Collections.unmodifiableMap(gauges);
    }
}
