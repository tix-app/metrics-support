package tix.metrics.codahale.collector;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.Metric;
import com.codahale.metrics.MetricSet;
import com.codahale.metrics.RatioGauge;
import tix.metrics.MetricsKeyBuilder;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryPoolMXBean;
import java.lang.management.MemoryUsage;
import java.util.*;
import java.util.regex.Pattern;

/**
 * A set of gauges for JVM memory usage, including stats on heap vs. non-heap memory, plus GC-specific memory pools.
 */
public class MemoryUsageGaugeSet implements MetricSet {
    private static final Pattern WHITESPACE = Pattern.compile("[\\s]+");

    private final MetricsKeyBuilder metricsKeyBuilder = MetricsKeyBuilder
            .create()
            .withInstrumentedSection("jvm")
            .withTarget("memory");

    private final MemoryMXBean mxBean;
    private final List<MemoryPoolMXBean> memoryPools;

    public MemoryUsageGaugeSet() {
        this(ManagementFactory.getMemoryMXBean(), ManagementFactory.getMemoryPoolMXBeans());
    }

    public MemoryUsageGaugeSet(MemoryMXBean mxBean, Collection<MemoryPoolMXBean> memoryPools) {
        this.mxBean = mxBean;
        this.memoryPools = new ArrayList<>(memoryPools);
    }

    private String metricsKey(String action) {
        return metricsKeyBuilder.buildWithAction(action).success();
    }

    @Override
    public Map<String, Metric> getMetrics() {
        final Map<String, Metric> gauges = new HashMap<String, Metric>();

        gauges.put(metricsKey("total.init"), (Gauge<Long>) () ->
                mxBean.getHeapMemoryUsage().getInit() + mxBean.getNonHeapMemoryUsage().getInit());
        gauges.put(metricsKey("total.used"), (Gauge<Long>) () ->
                mxBean.getHeapMemoryUsage().getUsed() + mxBean.getNonHeapMemoryUsage().getUsed());
        gauges.put(metricsKey("total.max"), (Gauge<Long>) () ->
                mxBean.getHeapMemoryUsage().getMax() + mxBean.getNonHeapMemoryUsage().getMax());
        gauges.put(metricsKey("total.committed"), (Gauge<Long>) () ->
                mxBean.getHeapMemoryUsage().getCommitted() + mxBean.getNonHeapMemoryUsage().getCommitted());


        gauges.put(metricsKey("heap.init"), (Gauge<Long>) () -> mxBean.getHeapMemoryUsage().getInit());
        gauges.put(metricsKey("heap.used"), (Gauge<Long>) () -> mxBean.getHeapMemoryUsage().getUsed());
        gauges.put(metricsKey("heap.max"), (Gauge<Long>) () -> mxBean.getHeapMemoryUsage().getMax());
        gauges.put(metricsKey("heap.committed"), (Gauge<Long>) () -> mxBean.getHeapMemoryUsage().getCommitted());
        gauges.put(metricsKey("heap.usage"), new RatioGauge() {
            @Override
            protected Ratio getRatio() {
                final MemoryUsage usage = mxBean.getHeapMemoryUsage();
                return Ratio.of(usage.getUsed(), usage.getMax());
            }
        });

        gauges.put(metricsKey("non-heap.init"), (Gauge<Long>) () -> mxBean.getNonHeapMemoryUsage().getInit());
        gauges.put(metricsKey("non-heap.used"), (Gauge<Long>) () -> mxBean.getNonHeapMemoryUsage().getUsed());
        gauges.put(metricsKey("non-heap.max"), (Gauge<Long>) () -> mxBean.getNonHeapMemoryUsage().getMax());
        gauges.put(metricsKey("non-heap.committed"), (Gauge<Long>) () -> mxBean.getNonHeapMemoryUsage().getCommitted());
        gauges.put(metricsKey("non-heap.usage"), new RatioGauge() {
            @Override
            protected Ratio getRatio() {
                final MemoryUsage usage = mxBean.getNonHeapMemoryUsage();
                return Ratio.of(usage.getUsed(), usage.getMax());
            }
        });

        for (final MemoryPoolMXBean pool : memoryPools) {
            String poolName = WHITESPACE.matcher(pool.getName()).replaceAll("-");
            String metricsKey = metricsKey("pools." + poolName + ".usage");
            gauges.put(metricsKey, new RatioGauge() {
                @Override
                protected Ratio getRatio() {
                    final long max = pool.getUsage().getMax() == -1 ?
                            pool.getUsage().getCommitted() :
                            pool.getUsage().getMax();
                    return Ratio.of(pool.getUsage().getUsed(), max);
                }
            });
        }

        return Collections.unmodifiableMap(gauges);
    }
}
