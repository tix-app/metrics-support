package tix.metrics.codahale.collector;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;
import ch.qos.logback.core.UnsynchronizedAppenderBase;
import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.SharedMetricRegistries;
import org.apache.log4j.Appender;
import org.apache.log4j.Level;
import tix.metrics.MetricsKeyBuilder;

import static com.codahale.metrics.MetricRegistry.name;

/**
 * A Logback {@link ch.qos.logback.core.Appender} which has six meters, one for each logging level and one for the total
 * number of statements being logged. The meter names are the logging level names appended to the
 * name of the appender.
 */
public class MetricsLogbackAppender extends UnsynchronizedAppenderBase<ILoggingEvent> {
    private final MetricRegistry registry;

    protected final MetricsKeyBuilder metricsKeyBuilder;

    private Meter all;
    private Meter trace;
    private Meter debug;
    private Meter info;
    private Meter warn;
    private Meter error;
    private Meter fatal;

    /**
     * Create a new instrumented appender using the given registry.
     *
     * @param registry the metric registry
     */
    public MetricsLogbackAppender(MetricRegistry registry) {
        this.registry = registry;

        this.metricsKeyBuilder = MetricsKeyBuilder
                .create()
                .withInstrumentedSection("log")
                .withTarget("level");

        setName(name(Appender.class));
    }

    private String metricsKey(String action) {
        return metricsKeyBuilder.buildWithAction(action).success();
    }


    @Override
    public void start() {
        this.all = registry.meter(metricsKey("all"));
        this.trace = registry.meter(metricsKey("trace"));
        this.debug = registry.meter(metricsKey("debug"));
        this.info = registry.meter(metricsKey("info"));
        this.warn = registry.meter(metricsKey("warn"));
        this.error = registry.meter(metricsKey("error"));
        this.fatal = registry.meter(metricsKey("fatal"));
    }

    @Override
    protected void append(ILoggingEvent event) {
        all.mark();
        switch (event.getLevel().toInt()) {
            case Level.TRACE_INT:
                trace.mark();
                break;
            case Level.DEBUG_INT:
                debug.mark();
                break;
            case Level.INFO_INT:
                info.mark();
                break;
            case Level.WARN_INT:
                warn.mark();
                break;
            case Level.ERROR_INT:
                error.mark();
                break;
            case Level.FATAL_INT:
                fatal.mark();
                break;
            default:
                break;
        }
    }
}
