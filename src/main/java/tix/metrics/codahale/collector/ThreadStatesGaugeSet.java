package tix.metrics.codahale.collector;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.Metric;
import com.codahale.metrics.MetricSet;
import tix.metrics.MetricsKeyBuilder;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * A set of gauges for the number of threads in their various states and deadlock detection.
 */
public class ThreadStatesGaugeSet implements MetricSet {
    // do not compute stack traces.
    private final static int STACK_TRACE_DEPTH = 0;

    private final MetricsKeyBuilder metricsKeyBuilder = MetricsKeyBuilder
            .create()
            .withInstrumentedSection("jvm")
            .withTarget("thread");

    private final ThreadMXBean threads;
    private final ThreadDeadlockDetector deadlockDetector;

    /**
     * Creates a new set of gauges using the default MXBeans.
     */
    public ThreadStatesGaugeSet() {
        this(ManagementFactory.getThreadMXBean(), new ThreadDeadlockDetector());
    }

    /**
     * Creates a new set of gauges using the given MXBean and detector.
     *
     * @param threads          a thread MXBean
     * @param deadlockDetector a deadlock detector
     */
    public ThreadStatesGaugeSet(ThreadMXBean threads,
                                ThreadDeadlockDetector deadlockDetector) {
        this.threads = threads;
        this.deadlockDetector = deadlockDetector;
    }

    private String metricsKey(String action) {
        return metricsKeyBuilder.buildWithAction(action).success();
    }

    @Override
    public Map<String, Metric> getMetrics() {
        final Map<String, Metric> gauges = new HashMap<>();

        for (final Thread.State state : Thread.State.values()) {
            String threadState = state.toString().toLowerCase();
            gauges.put(metricsKey(threadState + ".count"),
                    (Gauge<Object>) () -> getThreadCount(state));
        }

        gauges.put(metricsKey("count"), (Gauge<Integer>) threads::getThreadCount);
        gauges.put(metricsKey("daemon.count"), (Gauge<Integer>) threads::getDaemonThreadCount);

        gauges.put(metricsKey("deadlocks"), (Gauge<Set<String>>) deadlockDetector::getDeadlockedThreads);

        return Collections.unmodifiableMap(gauges);
    }

    private int getThreadCount(Thread.State state) {
        final ThreadInfo[] allThreads = getThreadInfo();
        int count = 0;
        for (ThreadInfo info : allThreads) {
            if (info != null && info.getThreadState() == state) {
                count++;
            }
        }
        return count;
    }

    ThreadInfo[] getThreadInfo() {
        return threads.getThreadInfo(threads.getAllThreadIds(), STACK_TRACE_DEPTH);
    }
}
