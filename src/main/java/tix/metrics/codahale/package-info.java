/**
 * <p/>
 * Creates a ScheduledReporter that works closely with a MetricsRegistry. A MetricsRegistry
 * without a ScheduledReporter is simply collecting all sorts of Metrics but is never sent
 * to any stats server. The Reporter defines when to send collected metrics and where to
 * send it to.
 * <p/>
 * Currently we support 2 types {@link nl.chess.it.codahale.ScheduledReporterFactoryBean.Type#statsd}
 * and {@link nl.chess.it.codahale.ScheduledReporterFactoryBean.Type#graphite}.
 * In order to have Graphite and Statsd up-and-running locally, on test and product, please have a
 * look at Chessforge:
 * <pre>
 *     https://chessforge.chess-ix.com/gf/project/metrics/
 * </pre>
 * It contains a special Git repository helping you to setup your environment in a (relatively)
 * easy way.
 * <pre>
 *     git clone ssh://{username}@chessforge.chess-ix.com/gitroot/metrics
 * </pre>
 * <p/>
 * It typically does this through Statsd reporter
 * {@link nl.chess.it.codahale.ScheduledReporterFactoryBean#statsd(com.codahale.metrics.MetricRegistry, nl.chess.it.ServerAddress)}
 * or directly to Graphite without any Statsd pre-processing using
 * {@link nl.chess.it.codahale.ScheduledReporterFactoryBean#graphite(com.codahale.metrics.MetricRegistry, nl.chess.it.ServerAddress)}.
 * <p/>
 * Read more about Statsd here:
 * <pre>
 *     https://github.com/etsy/statsd
 *     https://www.datadoghq.com/2013/08/statsd/
 * </pre>
 */
package tix.metrics.codahale;