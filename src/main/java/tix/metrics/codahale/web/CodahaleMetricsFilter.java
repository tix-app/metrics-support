package tix.metrics.codahale.web;

import com.codahale.metrics.Counter;
import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.http.HttpStatus;
import org.springframework.web.filter.GenericFilterBean;
import tix.metrics.MetricsKeyBuilder;
import tix.metrics.codahale.collector.*;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static org.springframework.http.HttpStatus.*;

/**
 * {@link javax.servlet.Filter} implementation which captures request information and a breakdown of the response
 * codes being returned.
 */
@Slf4j
public class CodahaleMetricsFilter extends GenericFilterBean implements ApplicationListener<ContextRefreshedEvent> {
    private MetricRegistry metricsRegistry;

    private MetricsKeyBuilder httpStatusKeyBuilder;
    private MetricsKeyBuilder httpRequestKeyBuilder;
    private ConcurrentMap<Integer, Meter> metersByStatusCode = new ConcurrentHashMap<Integer, Meter>(6);
    private Meter otherMeter;
    private Counter activeRequests;
    private Timer requestTimer;

    public CodahaleMetricsFilter(MetricRegistry metricsRegistry) {
        this.metricsRegistry = metricsRegistry;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (httpStatusKeyBuilder != null) {
            log.debug("MetricsFilter already configured, ignoring startup event.");
            return;
        }

        httpRequestKeyBuilder = MetricsKeyBuilder
                .create()
                .withInstrumentedSection("http")
                .withTarget("request");
        httpStatusKeyBuilder = MetricsKeyBuilder
                .create()
                .withInstrumentedSection("http")
                .withTarget("status");

        addMeterByStatusCode(OK);           // 200
        addMeterByStatusCode(CREATED);      // 201
        addMeterByStatusCode(NO_CONTENT);   // 204
        addMeterByStatusCode(BAD_REQUEST);  // 400
        addMeterByStatusCode(NOT_FOUND);    // 404
        addMeterByStatusCode(INTERNAL_SERVER_ERROR); // 500

        otherMeter = metricsRegistry.meter(httpRequestKeyBuilder.buildWithAction("other").success());
        activeRequests = metricsRegistry.counter(httpRequestKeyBuilder.buildWithAction("activeRequests").success());
        requestTimer = metricsRegistry.timer(httpRequestKeyBuilder.buildWithAction("requests").success());
    }

    protected String name(String name) {
        return httpStatusKeyBuilder.buildWithAction(name).success();
    }

    protected void addMeterByStatusCode(HttpStatus httpStatus) {
        String name = name("" + httpStatus.value());
        Meter meter = metricsRegistry.meter(name);
        metersByStatusCode.put(httpStatus.value(), meter);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        final StatusExposingServletResponse wrappedResponse = new StatusExposingServletResponse((HttpServletResponse) response);
        activeRequests.inc();
        final Timer.Context context = requestTimer.time();
        try {
            chain.doFilter(request, wrappedResponse);
        } finally {
            context.stop();
            activeRequests.dec();
            markMeterForStatusCode(wrappedResponse.getStatus());
        }
    }

    private void markMeterForStatusCode(int status) {
        final Meter metric = metersByStatusCode.get(status);
        if (metric != null) {
            metric.mark();
        } else {
            otherMeter.mark();
        }
    }

    private static class StatusExposingServletResponse extends HttpServletResponseWrapper {
        // The Servlet spec says: calling setStatus is optional, if no status is set, the default is 200.
        private int httpStatus = 200;

        public StatusExposingServletResponse(HttpServletResponse response) {
            super(response);
        }

        @Override
        public void sendError(int sc) throws IOException {
            httpStatus = sc;
            super.sendError(sc);
        }

        @Override
        public void sendError(int sc, String msg) throws IOException {
            httpStatus = sc;
            super.sendError(sc, msg);
        }

        @Override
        public void setStatus(int sc) {
            httpStatus = sc;
            super.setStatus(sc);
        }

        public int getStatus() {
            return httpStatus;
        }
    }
}
