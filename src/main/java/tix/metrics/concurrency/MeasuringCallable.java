package tix.metrics.concurrency;

import lombok.extern.slf4j.Slf4j;
import tix.metrics.annotation.Timed;

import java.util.concurrent.Callable;

@Slf4j
public abstract class MeasuringCallable<V> implements Callable<V> {
    @Timed
    @Override
    public final V call() {
        try {
            return doCall();
        } catch (Exception e) {
            /*
             * Normally considered uncustomary, but since it's a background
             * thread both we must log and rethrow the exception.
             */
            log.error("Could not execute " + getClass().getName(), e);
            throw new RuntimeException("Error executing background thread", e);
        }
    }

    protected abstract V doCall() throws Exception;
}
