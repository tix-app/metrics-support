package tix.metrics.namespace;

import static org.springframework.util.StringUtils.hasText;

public class FixedNamespaceResolver implements NamespaceResolver {
    private final String namespace;

    public FixedNamespaceResolver(String namespace) {
        assert namespace != null && hasText(namespace);
        this.namespace = namespace;
    }

    @Override
    public String namespace() {
        return namespace;
    }
}
