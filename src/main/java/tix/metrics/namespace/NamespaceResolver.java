package tix.metrics.namespace;

/**
 * Please see Javadoc of {@link nl.chess.it.MetricsKeyBuilder}
 *
 * Implementations of this interface handle creation of the <namespace> part of the MetricsKey.
 */
public interface NamespaceResolver {
    String namespace();
}
