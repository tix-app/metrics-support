package tix.metrics.namespace;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static java.time.LocalDateTime.now;
import static java.time.format.DateTimeFormatter.ofPattern;

@Configurable(autowire = Autowire.BY_NAME, preConstruction = true)
public class UniquePerRestartNamespaceResolver implements NamespaceResolver {
    @Autowired
    Clock clock;

    NamespaceResolver delegate;

    public UniquePerRestartNamespaceResolver(NamespaceResolver delegate) {
        this.delegate = delegate;
    }

    @Override
    public String namespace() {
        String namespace = delegate.namespace();
        return namespace + "-" + now(clock).format(ofPattern("yyyyMMdd-HHmm"));
    }
}
