package tix.metrics.namespace;

import static org.springframework.util.StringUtils.hasText;

public class UsernameBasedNamespaceResolver implements NamespaceResolver {
    @Override
    public String namespace() {
        String userName = System.getProperty("user.name");
        return hasText(userName) ? userName : "unknown";
    }
}
