/**
 * <p>
 * Standalone module for guaranteeing NON-blocking storage
 * of metrics. It will never throw an exception.<br/>
 * <br/>
 * Metrics implementations must be thread-safe.
 * </p>
 * <p>
 * In DDD terms, the Metrics is acting as the aggregate root.
 * </p>
 * <p>
 * All actions should be performed on Metrics, which will transparantly
 * handle NON-blocking sending of metrics to whichever underlying mechanism
 * (typically Graphite-Statsd though).
 * </p>
 * <p>
 * Metrics can be used separately or in conjunction with e.g. {@link nl.chess.it.annotation.Timed}
 * icw. {@link nl.chess.it.annotation.CountedAspect}.
 * </p>
 */
package tix.metrics;