package tix.metrics.sigar;

import org.hyperic.sigar.Sigar;
import tix.metrics.MetricsKeyBuilder;

abstract class AbstractSigarMetric implements CanRegisterGauges {
    protected final MetricsKeyBuilder metricsKeyBuilder = MetricsKeyBuilder
            .create()
            .withInstrumentedSection("os");

    protected final Sigar sigar;

    protected AbstractSigarMetric(Sigar sigar) {
        this.sigar = sigar;
    }
}
