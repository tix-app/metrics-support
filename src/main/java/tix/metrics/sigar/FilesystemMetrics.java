package tix.metrics.sigar;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;
import lombok.extern.slf4j.Slf4j;
import org.hyperic.sigar.FileSystem;
import org.hyperic.sigar.FileSystemUsage;
import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;
import tix.metrics.MetricsKeyBuilder;

@Slf4j
public class FilesystemMetrics extends AbstractSigarMetric {
    private final MetricsKeyBuilder fsMetricsKeyBuilder = metricsKeyBuilder.withTarget("fs");
    private final FileSystem[] fileSystems;

    public enum FSType {
        // Ordered so that ordinals match the constants
        // in org.hyperic.sigar.FileSystem
        Unknown, None, LocalDisk, Network, Ramdisk, Cdrom, Swap
    }

    protected FilesystemMetrics(Sigar sigar) {
        super(sigar);

        fileSystems = filesystems();
    }

    private String metricsKey(String action) {
        return fsMetricsKeyBuilder.buildWithAction(action).success();
    }

    public void registerGauges(MetricRegistry registry) {
        for (final FileSystem fileSystem : fileSystems) {
            registry.register(metricsKey(fileSystem.getDirName() + ".percentage-full"), new Gauge<Double>() {
                @Override
                public Double getValue() {
                    FileSystemSummary summary = generateSummary(fileSystem);
                    return summary.percentageFull();
                }
            });
        }
    }

    public FileSystem[] filesystems() {
        try {
            return sigar.getFileSystemList();
        } catch (SigarException e) {
            log.debug("Could not check file system list", e);
            return new FileSystem[]{};
        }
    }

    public FileSystemSummary generateSummary(FileSystem fs) {
        String dirName = fs.getDirName();
        long totalSizeKB = 0L;
        long freeSpaceKB = 0L;
        try {
            FileSystemUsage usage = sigar.getFileSystemUsage(dirName);
            totalSizeKB = usage.getTotal();
            freeSpaceKB = usage.getFree();
        } catch (SigarException e) {
            log.debug("Could not generate FS summary", e);
        }
        return FileSystemSummary.fromSigarBean(fs, totalSizeKB, freeSpaceKB);
    }

    public static final class FileSystemSummary {
        private final String deviceName;
        private final String mountPoint;
        private final FSType genericFSType;
        private final String osSpecificFSType;
        private final long totalSizeKB;
        private final long freeSpaceKB;

        public FileSystemSummary(String deviceName, String mountPoint, FSType genericFSType, String osSpecificFSType, long totalSizeKB, long freeSpaceKB) {
            this.deviceName = deviceName;
            this.mountPoint = mountPoint;
            this.genericFSType = genericFSType;
            this.osSpecificFSType = osSpecificFSType;
            this.totalSizeKB = totalSizeKB;
            this.freeSpaceKB = freeSpaceKB;
        }

        public static FileSystemSummary fromSigarBean(org.hyperic.sigar.FileSystem fs, long totalSizeKB, long freeSpaceKB) {
            return new FileSystemSummary(fs.getDevName(), fs.getDirName(), FSType.values()[fs.getType()], fs.getSysTypeName(), totalSizeKB, freeSpaceKB);
        }

        public String deviceName() { return deviceName; }
        public String mountPoint() { return mountPoint; }
        public FSType genericFSType() { return genericFSType; }
        public String osSpecificFSType() { return osSpecificFSType; }
        public long totalSizeKB() { return totalSizeKB; }
        public long freeSpaceKB() { return freeSpaceKB; }
        public double percentageFull() {
            if (totalSizeKB == 0L) {
                return 0;
            }
            return (double)freeSpaceKB / totalSizeKB;
        }
    }
}
