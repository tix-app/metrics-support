package tix.metrics.sigar;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;
import org.hyperic.sigar.Sigar;
import tix.metrics.MetricsKey;

public class PidMetrics extends AbstractSigarMetric {
    private MetricsKey metricsKey;

    protected PidMetrics(Sigar sigar) {
        super(sigar);
    }

    public MetricsKey metricsKey() {
        if (metricsKey == null) {
            metricsKey = metricsKeyBuilder.withTarget("pid").build();
        }
        return metricsKey;
    }

    public void registerGauges(MetricRegistry registry) {
        registerPid(registry, metricsKey().success());
    }

    public void registerPid(MetricRegistry registry, String name) {
        registry.register(name, new Gauge<Long>() {
            public Long getValue() {
                return pid();
            }
        });

    }

    private long pid() {
        return sigar.getPid();
    }
}
