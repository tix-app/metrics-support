package tix.metrics.sigar;

import com.codahale.metrics.MetricRegistry;
import org.hyperic.sigar.Sigar;

/**
 * Based on:
  <dependency>
     <groupId>com.github.cb372</groupId>
     <artifactId>metrics-sigar</artifactId>
     <version>0.2.2</version>
  </dependency>

 * The original wasn't very extensible, which was mostly annoying for the metrics keys since
 * they'd generate long long paths instead of simple ones.
 */
public class SigarMetrics implements CanRegisterGauges {
    Sigar sigar;
    PidMetrics pid;
    CpuMetrics cpu;
    MemoryMetrics memory;
    FilesystemMetrics fs;
    UlimitMetrics ulimit;

    public SigarMetrics() {
        sigar = new Sigar();
        pid = new PidMetrics(sigar);
        cpu = new CpuMetrics(sigar);
        memory = new MemoryMetrics(sigar);
        fs = new FilesystemMetrics(sigar);
        ulimit = new UlimitMetrics(sigar);
    }

    @Override
    public void registerGauges(MetricRegistry registry) {
        pid.registerGauges(registry);
        cpu.registerGauges(registry);
        memory.registerGauges(registry);
        fs.registerGauges(registry);
        ulimit.registerGauges(registry);
    }

    public CpuMetrics cpu() {
        return cpu;
    }

    public MemoryMetrics memory() {
        return memory;
    }

    public FilesystemMetrics filesystems() {
        return fs;
    }

    public UlimitMetrics ulimit() {
        return ulimit;
    }
}
