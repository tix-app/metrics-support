package tix.metrics.sigar.detect;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import tix.metrics.MetricsException;

import static tix.metrics.OperatingSystem.*;
import static tix.metrics.sigar.detect.Arch.amd;
import static tix.metrics.sigar.detect.Arch.x86;
import static tix.metrics.sigar.detect.Os.*;

@Slf4j
@EqualsAndHashCode
@ToString(includeFieldNames = false)
class Combination {
    private final Os os;
    private final Arch arch;
    private final String nativeLibrary;

    public Combination(Os os, Arch arch, String nativeLibrary) {
        this.os = os;
        this.arch = arch;
        this.nativeLibrary = nativeLibrary;
    }

    public boolean matchesCurrentHost() {
        Os currentOs = currentOs();
        Arch currentArch = currentArch();
        return currentOs == os && currentArch == arch;
    }

    private Os currentOs() {
        if (isLinux()) {
            return linux;
        }
        if (isWindows()) {
            return windows;
        }
        if (isOsx()) {
            return osx;
        }
        throw new MetricsException("Unsupported operating system " + name());
    }

    private Arch currentArch() {
        if (isAmd()) {
            return amd;
        }
        return x86;
    }

    public String nativeLibraryPath() {
        return nativeLibrary;
    }
}
