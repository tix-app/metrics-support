package tix.metrics.sigar.detect;

public class LoaderResult {
    boolean success;
    Exception exception;

    public static LoaderResult success() {
        return new LoaderResult(true);
    }

    public static LoaderResult failure(Exception exception) {
        return new LoaderResult(false);
    }

    public static LoaderResult noMatch() {
        return new LoaderResult(false);
    }

    private LoaderResult(boolean success) {
        this.success = success;
    }

    protected LoaderResult exception(final Exception exception) {
        this.exception = exception;
        return this;
    }

    public boolean isSuccess() {
        return success;
    }

    public Exception exception() {
        return exception;
    }
}
