package tix.metrics.sigar.detect;

public interface NativeSigarLoader {
    LoaderResult loadNativeLibrary();
}
