package tix.metrics.sigar.detect;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.Set;

import static com.google.common.collect.Sets.newHashSet;
import static tix.metrics.sigar.detect.Arch.amd;
import static tix.metrics.sigar.detect.Arch.x86;
import static tix.metrics.sigar.detect.LoaderResult.noMatch;
import static tix.metrics.sigar.detect.Os.*;

@Slf4j
public class OsAwareNativeSigarLoader implements NativeSigarLoader {
    final static String sigarLibraryBasePath = "/org/hyperic/sigar/1.6.4";

    Set<Combination> possibleCombinations = newHashSet(
            new Combination(linux, amd, "libsigar-amd64-linux.so"),
            new Combination(linux, x86, "libsigar-x86-linux.so"),
            new Combination(windows, amd, "sigar-amd64-winnt.dll"),
            new Combination(windows, x86, "sigar-x86-winnt.dll"),
            new Combination(osx, x86, "libsigar-universal64-macosx.dylib"));

    public LoaderResult loadNativeLibrary() {
        for (Combination possibleCombination : possibleCombinations) {
            if (possibleCombination.matchesCurrentHost()) {
                log.info("Detected native Sigar library based on current operating system: [{}]", possibleCombination);

                Resource resource = new ClassPathResource(sigarLibraryBasePath + "/" + possibleCombination.nativeLibraryPath());
                log.debug("Verifying existence of native Sigar library [{}]", resource);

                if (resource.exists()) {
                    return load(resource);
                }
            }
        }
        log.warn("Could not initialize Sigar, no native Sigar library found. System level metrics will not be generated. " +
                "\nPlease make sure that SigarNativeLibraryDetector#possibleCombinations contains a possible combination " +
                "for your current operating system and architecture.");
        return noMatch();
    }

    private LoaderResult load(Resource resource) {
        try {
            Runtime.getRuntime().load(resource.getFile().getAbsolutePath());
            return LoaderResult.success();
        } catch (IOException e) {
            log.warn("Could not load native Sigar library", e);
            return LoaderResult.failure(e);
        }
    }


}
