package tix.metrics.time;

import com.google.common.base.Stopwatch;

import java.util.concurrent.TimeUnit;

public class Duration {
    private final long duration;
    private final TimeUnit unit;

    public static Duration millis(long timeout) {
        return new Duration(timeout, TimeUnit.MILLISECONDS);
    }

    public static Duration seconds(long timeout) {
        return new Duration(timeout, TimeUnit.SECONDS);
    }

    public static Duration from(Stopwatch stopwatch) {
        return millis(stopwatch.elapsed(TimeUnit.MILLISECONDS));
    }

    private Duration(long duration, TimeUnit unit) {
        this.duration = duration;
        this.unit = unit;
    }

    public long toMillis() {
        return unit.toMillis(duration);
    }

}
