package tix.metrics.time;

import java.util.concurrent.TimeUnit;

public class Interval {
    private final long interval;
    private final TimeUnit unit;

    public static Interval millis(long interval) {
        return new Interval(interval, TimeUnit.MILLISECONDS);
    }

    public static Interval seconds(long interval) {
        return new Interval(interval, TimeUnit.SECONDS);
    }

    public static Interval minutes(long interval) {
        return new Interval(interval, TimeUnit.MINUTES);
    }

    public static Interval from(long value, TimeUnit unit) {
        return new Interval(value, unit);
    }

    private Interval(long interval, TimeUnit unit) {
        this.interval = interval;
        this.unit = unit;
    }

    public long toMillis() {
        return unit.toMillis(interval);
    }

    public long toSeconds() {
        return unit.toSeconds(interval);
    }

    public long toMinutes() {
        return unit.toMinutes(interval);
    }
}
