package tix.metrics;

import org.junit.Before;
import org.junit.Test;
import tix.metrics.codahale.collector.AbstractMetricsTest;
import tix.metrics.namespace.NamespaceResolver;
import tix.test.autowire.AutowiringTest;

import static org.junit.Assert.*;

public class MetricsKeyBuilderTest extends AutowiringTest {
    @Before
    public void beforeAnyMetricsCreation() {
        registerBean("namespaceResolver", (NamespaceResolver) () -> "keybuilder");
    }

    @Test
    public void testBuildWithAction() throws Exception {
        MetricsKeyBuilder builder = MetricsKeyBuilder
                .create()
                .withInstrumentedSection("instrumented")
                .withTarget("target");
        assertEquals("keybuilder.instrumented.target.action1", builder
                .buildWithAction("action1")
                .success());
        assertEquals("keybuilder.instrumented.target.action2", builder
                .buildWithAction("action2")
                .success());
    }

    @Test
    public void testBuild() throws Exception {
        assertEquals("keybuilder.instrumented.target.action", MetricsKeyBuilder
                .create()
                .withInstrumentedSection("instrumented")
                .withTarget("target")
                .withAction("action")
                .build()
                .success());
    }
}