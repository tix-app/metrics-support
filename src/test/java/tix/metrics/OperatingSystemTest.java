package tix.metrics;

import org.junit.Test;

import static org.junit.Assert.*;

public class OperatingSystemTest {

    @Test
    public void testIsAmd() throws Exception {
        System.setProperty("os.arch", "armv41");
        assertFalse(OperatingSystem.isAmd());

        System.setProperty("os.arch", "i386");
        assertFalse(OperatingSystem.isAmd());

        System.setProperty("os.arch", "amd64");
        assertTrue(OperatingSystem.isAmd());
    }

    @Test
    public void testIsX86() throws Exception {
        System.setProperty("os.arch", "armv41");
        assertFalse(OperatingSystem.isX86());

        System.setProperty("os.arch", "i386");
        assertTrue(OperatingSystem.isX86());

        System.setProperty("os.arch", "amd64");
        assertFalse(OperatingSystem.isX86());
    }
}