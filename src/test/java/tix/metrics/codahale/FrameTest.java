package tix.metrics.codahale;

import org.junit.Before;
import org.junit.Test;
import tix.logging.Logback;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static tix.metrics.codahale.Frame.NEWLINE_BYTE;

public class FrameTest {
    @Before
    public void setUp() {
        Logback.unitTests();
    }

    @Test
    public void testNextBytes() throws Exception {
        Frame frame = new Frame(Mtu.customMtu(128, 108));
        String tenChars = "123456789" + new String(new byte[] {NEWLINE_BYTE});
        String fiftyChars = tenChars + tenChars + tenChars + tenChars + tenChars;
        String hundredChars = fiftyChars + fiftyChars;

        // Create a packet with 310 characters
        frame.append(hundredChars, hundredChars, hundredChars, tenChars);

        /*
         * Only split on new line, never exceed the max MTU, and stay as close to MTU.suggestedCutoff()
         * as we can. So, 310 should be split into 3 windows with sizes 110, 110, 90 respectively
         */
        String hundredTenChars = hundredChars + tenChars;
        String ninetyChars = fiftyChars + tenChars + tenChars + tenChars + tenChars;

        String[] expectedContent = new String[]{hundredTenChars, hundredTenChars, ninetyChars};

        int counter = 0;
        while (frame.hasMorePackets()) {
            byte[] bytes = frame.nextPacket();

            if (counter > expectedContent.length) {
                fail("Packet has been giving more bytes then expected, " + counter + " >= " + expectedContent.length);
            }
            String expected = expectedContent[counter];
            String actual = new String(bytes);

            /*
             * Convert to String to allow for easy visual comparison
             */
            assertEquals(expected, actual);

            counter++;
        }
        /*
         * Should've created 3 byte-arrays.
         */
        assertEquals(3, counter);
    }

    @Test
    public void testNextBytesTwice() throws Exception {
        testNextBytes();
        testNextBytes();
    }
}
