package tix.metrics.codahale;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;
import tix.logging.Logback;
import tix.metrics.OperatingSystem;
import tix.metrics.ServerAddress;

import static org.junit.Assert.assertEquals;

public class StatsdTest {
    int udpPort = 50000;

    UdpServer udpServer;
    Mtu mtu;

    @Before
    public void setUp() {
        Logback.unitTests();
        
        mtu = Mtu.statsdMtu();
        udpServer = UdpServer.start(mtu, udpPort);
    }

    @After
    public void tearDown() throws Exception {
        if (udpServer != null) {
            udpServer.stop();
        }
    }

    @Test
    public void testCorrectSplittingPackagesExceedingMtu() throws Exception {
        /*
         * Workaround discussed with Rolf since only Windows environments seem to
         * break. We think it has something to do with either firewall settings or
         * different max packet size settings. Not highest priority to fix.
         */
        if (OperatingSystem.isWindows()) {
            return;
        }

        Resource resource = new ClassPathResource("/codahale/packet-large1.txt");
        String entirePacket = new String(FileCopyUtils.copyToByteArray(resource.getInputStream()));
        String[] lines = StringUtils.tokenizeToStringArray(entirePacket, "\n");


        Statsd statsd = new Statsd(new ServerAddress("localhost", udpPort));
        for (String line : lines) {
            String[] tokens = StringUtils.tokenizeToStringArray(line, ":|");
            assert tokens.length == 3;

            statsd.send(tokens[0], tokens[1], StatType.fromWireValue(tokens[2]));
        }
        statsd.close();

        /*
         * Let server catch up with packets
         */
        Thread.sleep(1500);

        String actualReceivedContent = udpServer.incomingDataConcatenated();
        assertEquals(entirePacket, actualReceivedContent);
    }
}
