package tix.metrics.codahale;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.springframework.util.StringUtils;
import tix.logging.Logback;
import tix.metrics.MetricsException;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.google.common.collect.Lists.newArrayList;

@Slf4j
public class UdpServer {
    final Mtu mtu;
    final int port;

    final AtomicBoolean isRunning = new AtomicBoolean(false);
    final List<String> incomingData = newArrayList();

    @Before
    public void setUp() {
        Logback.unitTests();
    }

    public static UdpServer start(Mtu mtu, int port) {
        return new UdpServer(mtu, port);
    }

    private UdpServer(Mtu mtu, int port) {
        this.mtu = mtu;
        this.port = port;

        new Thread() {
            @Override
            public void run() {
                try {
                    waitForIncomingData();
                } catch (Exception e) {
                    throw new MetricsException("Exception while waiting for data", e);
                }
            }
        }.start();
    }

    private void waitForIncomingData() throws Exception {
        log.info("Server starting ...\n");

        isRunning.set(true);

        // Create a datagram socket bound to port 10000. Datagram
        // packets sent from client programs arrive at this port.

        DatagramSocket s = new DatagramSocket(port);

        while (isRunning.get()) {
            // Create a byte array to hold data contents of datagram
            // packet.
            byte[] data = new byte[mtu.maxSize()];

            // Create a DatagramPacket object that encapsulates a reference
            // to the byte array and destination address information. The
            // DatagramPacket object is not initialized to an address
            // because it obtains that address from the client program.
            DatagramPacket dgp = new DatagramPacket(data, data.length);

            // Receive a datagram packet from the client program.
            s.receive(dgp);

            // Display contents of datagram packet.
            String packet = new String(data).replaceAll("\\u0000", "");
            log.debug("Received {} bytes [{}]", packet.length(), packet);

            incomingData.add(packet);
            sleep(20);
        }
    }

    public static void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new IllegalStateException("Should not have exceptions like these when running a unit test", e);
        }
    }

    public UdpServer stop() throws Exception {
        isRunning.set(false);
        return this;
    }

    public List<String> incomingData() {
        return newArrayList(incomingData);
    }

    public String incomingDataConcatenated() {
        return StringUtils.collectionToDelimitedString(incomingData, "");
    }
}