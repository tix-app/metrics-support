package tix.metrics.codahale.collector;

import org.junit.Before;
import tix.metrics.namespace.NamespaceResolver;
import tix.test.autowire.AutowiringTest;

public abstract class AbstractMetricsTest extends AutowiringTest {
    @Before
    public void beforeAnyMetricsCreation() {
        registerBean("namespaceResolver", (NamespaceResolver) () -> null);
    }
}
