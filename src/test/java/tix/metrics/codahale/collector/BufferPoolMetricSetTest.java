package tix.metrics.codahale.collector;

import com.codahale.metrics.Gauge;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.management.InstanceNotFoundException;
import javax.management.MBeanServer;
import javax.management.ObjectName;

import static org.assertj.core.api.Assertions.assertThat;

public class BufferPoolMetricSetTest extends AbstractMetricsTest {
    private MBeanServer mBeanServer = Mockito.mock(MBeanServer.class);
    private BufferPoolMetricSet buffers;

    private ObjectName mapped;
    private ObjectName direct;

    @Before
    public void setUp() throws Exception {
        this.mapped = new ObjectName("java.nio:type=BufferPool,name=mapped");
        this.direct = new ObjectName("java.nio:type=BufferPool,name=direct");
        this.buffers = new BufferPoolMetricSet(mBeanServer);
    }

    @Test
    public void includesGaugesForDirectAndMappedPools() throws Exception {
        assertThat(buffers.getMetrics().keySet())
                .containsOnly(
                        "jvm.bufferpool.direct.count",
                        "jvm.bufferpool.mapped.used",
                        "jvm.bufferpool.mapped.capacity",
                        "jvm.bufferpool.direct.capacity",
                        "jvm.bufferpool.mapped.count",
                        "jvm.bufferpool.direct.used");
    }

    @Test
    public void ignoresGaugesForObjectsWhichCannotBeFound() throws Exception {
        Mockito.when(mBeanServer.getMBeanInfo(mapped)).thenThrow(new InstanceNotFoundException());

        assertThat(buffers.getMetrics().keySet())
                .containsOnly(
                        "jvm.bufferpool.direct.count",
                        "jvm.bufferpool.direct.capacity",
                        "jvm.bufferpool.direct.used");
    }

    @Test
    public void includesAGaugeForDirectCount() throws Exception {
        final Gauge gauge = (Gauge) buffers.getMetrics().get("jvm.bufferpool.direct.count");

        Mockito.when(mBeanServer.getAttribute(direct, "Count")).thenReturn(100);

        assertThat(gauge.getValue())
                .isEqualTo(100);
    }

    @Test
    public void includesAGaugeForDirectMemoryUsed() throws Exception {
        final Gauge gauge = (Gauge) buffers.getMetrics().get("jvm.bufferpool.direct.used");

        Mockito.when(mBeanServer.getAttribute(direct, "MemoryUsed")).thenReturn(100);

        assertThat(gauge.getValue())
                .isEqualTo(100);
    }

    @Test
    public void includesAGaugeForDirectCapacity() throws Exception {
        final Gauge gauge = (Gauge) buffers.getMetrics().get("jvm.bufferpool.direct.capacity");

        Mockito.when(mBeanServer.getAttribute(direct, "TotalCapacity")).thenReturn(100);

        assertThat(gauge.getValue())
                .isEqualTo(100);
    }

    @Test
    public void includesAGaugeForMappedCount() throws Exception {
        final Gauge gauge = (Gauge) buffers.getMetrics().get("jvm.bufferpool.mapped.count");

        Mockito.when(mBeanServer.getAttribute(mapped, "Count")).thenReturn(100);

        assertThat(gauge.getValue())
                .isEqualTo(100);
    }

    @Test
    public void includesAGaugeForMappedMemoryUsed() throws Exception {
        final Gauge gauge = (Gauge) buffers.getMetrics().get("jvm.bufferpool.mapped.used");

        Mockito.when(mBeanServer.getAttribute(mapped, "MemoryUsed")).thenReturn(100);

        assertThat(gauge.getValue())
                .isEqualTo(100);
    }

    @Test
    public void includesAGaugeForMappedCapacity() throws Exception {
        final Gauge gauge = (Gauge) buffers.getMetrics().get("jvm.bufferpool.mapped.capacity");

        Mockito.when(mBeanServer.getAttribute(mapped, "TotalCapacity")).thenReturn(100);

        assertThat(gauge.getValue())
                .isEqualTo(100);
    }
}