package tix.metrics.codahale.collector;

import com.codahale.metrics.Gauge;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.lang.management.ClassLoadingMXBean;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ClassLoadingGaugeSetTest extends AbstractMetricsTest {

    private final ClassLoadingMXBean cl = mock(ClassLoadingMXBean.class);
    private ClassLoadingGaugeSet gauges;

    @Before
    public void setUp() throws Exception {
        Mockito.when(cl.getTotalLoadedClassCount()).thenReturn(2L);
        Mockito.when(cl.getUnloadedClassCount()).thenReturn(1L);

        gauges = new ClassLoadingGaugeSet(cl);
    }

    @Test
    public void loadedGauge() throws Exception {
        final Gauge gauge = (Gauge) gauges.getMetrics().get("jvm.classloader.loaded");
        assertThat(gauge.getValue()).isEqualTo(2L);
    }

    @Test
    public void unLoadedGauge() throws Exception {
        final Gauge gauge = (Gauge) gauges.getMetrics().get("jvm.classloader.unloaded");
        assertThat(gauge.getValue()).isEqualTo(1L);
    }

}