package tix.metrics.codahale.collector;

import com.codahale.metrics.Gauge;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryPoolMXBean;
import java.lang.management.MemoryUsage;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MemoryUsageGaugeSetTest extends AbstractMetricsTest {
    private final MemoryUsage heap = mock(MemoryUsage.class);
    private final MemoryUsage nonHeap = mock(MemoryUsage.class);
    private final MemoryUsage pool = mock(MemoryUsage.class);
    private final MemoryUsage weirdPool = mock(MemoryUsage.class);
    private final MemoryMXBean mxBean = mock(MemoryMXBean.class);
    private final MemoryPoolMXBean memoryPool = mock(MemoryPoolMXBean.class);
    private final MemoryPoolMXBean weirdMemoryPool = mock(MemoryPoolMXBean.class);
    private MemoryUsageGaugeSet gauges;

    @Before
    public void setUp() throws Exception {
        when(heap.getCommitted()).thenReturn(10L);
        when(heap.getInit()).thenReturn(20L);
        when(heap.getUsed()).thenReturn(30L);
        when(heap.getMax()).thenReturn(40L);

        when(nonHeap.getCommitted()).thenReturn(1L);
        when(nonHeap.getInit()).thenReturn(2L);
        when(nonHeap.getUsed()).thenReturn(3L);
        when(nonHeap.getMax()).thenReturn(4L);

        when(pool.getCommitted()).thenReturn(100L);
        when(pool.getInit()).thenReturn(200L);
        when(pool.getUsed()).thenReturn(300L);
        when(pool.getMax()).thenReturn(400L);

        when(weirdPool.getCommitted()).thenReturn(100L);
        when(weirdPool.getInit()).thenReturn(200L);
        when(weirdPool.getUsed()).thenReturn(300L);
        when(weirdPool.getMax()).thenReturn(-1L);

        when(mxBean.getHeapMemoryUsage()).thenReturn(heap);
        when(mxBean.getNonHeapMemoryUsage()).thenReturn(nonHeap);

        when(memoryPool.getUsage()).thenReturn(pool);
        when(memoryPool.getName()).thenReturn("Big Pool");

        when(weirdMemoryPool.getUsage()).thenReturn(weirdPool);
        when(weirdMemoryPool.getName()).thenReturn("Weird Pool");

        gauges = new MemoryUsageGaugeSet(mxBean,
                Arrays.asList(memoryPool,
                        weirdMemoryPool));
    }

    @Test
    public void hasASetOfGauges() throws Exception {
        assertThat(gauges.getMetrics().keySet())
                .containsOnly(
                        "jvm.memory.heap.init",
                        "jvm.memory.non-heap.usage",
                        "jvm.memory.total.max",
                        "jvm.memory.total.init",
                        "jvm.memory.heap.committed",
                        "jvm.memory.non-heap.max",
                        "jvm.memory.heap.usage",
                        "jvm.memory.pools.Big-Pool.usage",
                        "jvm.memory.pools.Weird-Pool.usage",
                        "jvm.memory.heap.used",
                        "jvm.memory.non-heap.used",
                        "jvm.memory.non-heap.init",
                        "jvm.memory.total.used",
                        "jvm.memory.total.committed",
                        "jvm.memory.non-heap.committed",
                        "jvm.memory.heap.max");
    }

    @Test
    public void hasAGaugeForTotalCommitted() throws Exception {
        final Gauge gauge = (Gauge) gauges.getMetrics().get("jvm.memory.total.committed");

        assertThat(gauge.getValue())
                .isEqualTo(11L);
    }

    @Test
    public void hasAGaugeForTotalInit() throws Exception {
        final Gauge gauge = (Gauge) gauges.getMetrics().get("jvm.memory.total.init");

        assertThat(gauge.getValue())
                .isEqualTo(22L);
    }

    @Test
    public void hasAGaugeForTotalUsed() throws Exception {
        final Gauge gauge = (Gauge) gauges.getMetrics().get("jvm.memory.total.used");

        assertThat(gauge.getValue())
                .isEqualTo(33L);
    }

    @Test
    public void hasAGaugeForTotalMax() throws Exception {
        final Gauge gauge = (Gauge) gauges.getMetrics().get("jvm.memory.total.max");

        assertThat(gauge.getValue())
                .isEqualTo(44L);
    }

    @Test
    public void hasAGaugeForHeapCommitted() throws Exception {
        final Gauge gauge = (Gauge) gauges.getMetrics().get("jvm.memory.heap.committed");

        assertThat(gauge.getValue())
                .isEqualTo(10L);
    }

    @Test
    public void hasAGaugeForHeapInit() throws Exception {
        final Gauge gauge = (Gauge) gauges.getMetrics().get("jvm.memory.heap.init");

        assertThat(gauge.getValue())
                .isEqualTo(20L);
    }

    @Test
    public void hasAGaugeForHeapUsed() throws Exception {
        final Gauge gauge = (Gauge) gauges.getMetrics().get("jvm.memory.heap.used");

        assertThat(gauge.getValue())
                .isEqualTo(30L);
    }

    @Test
    public void hasAGaugeForHeapMax() throws Exception {
        final Gauge gauge = (Gauge) gauges.getMetrics().get("jvm.memory.heap.max");

        assertThat(gauge.getValue())
                .isEqualTo(40L);
    }

    @Test
    public void hasAGaugeForHeapUsage() throws Exception {
        final Gauge gauge = (Gauge) gauges.getMetrics().get("jvm.memory.heap.usage");

        assertThat(gauge.getValue())
                .isEqualTo(0.75);
    }

    @Test
    public void hasAGaugeForNonHeapCommitted() throws Exception {
        final Gauge gauge = (Gauge) gauges.getMetrics().get("jvm.memory.non-heap.committed");

        assertThat(gauge.getValue())
                .isEqualTo(1L);
    }

    @Test
    public void hasAGaugeForNonHeapInit() throws Exception {
        final Gauge gauge = (Gauge) gauges.getMetrics().get("jvm.memory.non-heap.init");

        assertThat(gauge.getValue())
                .isEqualTo(2L);
    }

    @Test
    public void hasAGaugeForNonHeapUsed() throws Exception {
        final Gauge gauge = (Gauge) gauges.getMetrics().get("jvm.memory.non-heap.used");

        assertThat(gauge.getValue())
                .isEqualTo(3L);
    }

    @Test
    public void hasAGaugeForNonHeapMax() throws Exception {
        final Gauge gauge = (Gauge) gauges.getMetrics().get("jvm.memory.non-heap.max");

        assertThat(gauge.getValue())
                .isEqualTo(4L);
    }

    @Test
    public void hasAGaugeForNonHeapUsage() throws Exception {
        final Gauge gauge = (Gauge) gauges.getMetrics().get("jvm.memory.non-heap.usage");

        assertThat(gauge.getValue())
                .isEqualTo(0.75);
    }

    @Test
    public void hasAGaugeForMemoryPoolUsage() throws Exception {
        final Gauge gauge = (Gauge) gauges.getMetrics().get("jvm.memory.pools.Big-Pool.usage");

        assertThat(gauge.getValue())
                .isEqualTo(0.75);
    }

    @Test
    public void hasAGaugeForWeirdMemoryPoolUsage() throws Exception {
        final Gauge gauge = (Gauge) gauges.getMetrics().get("jvm.memory.pools.Weird-Pool.usage");

        assertThat(gauge.getValue())
                .isEqualTo(3.0);
    }

    @Test
    public void autoDetectsMemoryUsageBeanAndMemoryPools() throws Exception {
        assertThat(new MemoryUsageGaugeSet().getMetrics().keySet())
                .isNotEmpty();
    }
}