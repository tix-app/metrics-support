package tix.metrics.codahale.collector;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import com.codahale.metrics.MetricRegistry;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MetricsLogbackAppenderTest extends AbstractMetricsTest {

    public static final String METRIC_NAME_PREFIX = "log.level";

    private MetricRegistry registry;
    private MetricsLogbackAppender appender;
    private ILoggingEvent event = mock(ILoggingEvent.class);

    @Before
    public void setUp() throws Exception {
        registry = new MetricRegistry();
        appender = new MetricsLogbackAppender(registry);
        appender.start();
    }

    @Test
    public void metersTraceEvents() throws Exception {
        when(event.getLevel()).thenReturn(Level.TRACE);

        appender.append(event);

        assertThat(registry.meter(METRIC_NAME_PREFIX + ".all").getCount()).isEqualTo(1);
        assertThat(registry.meter(METRIC_NAME_PREFIX + ".trace").getCount()).isEqualTo(1);
    }

    @Test
    public void metersDebugEvents() throws Exception {
        when(event.getLevel()).thenReturn(Level.DEBUG);

        appender.append(event);

        assertThat(registry.meter(METRIC_NAME_PREFIX + ".all").getCount()).isEqualTo(1);
        assertThat(registry.meter(METRIC_NAME_PREFIX + ".debug").getCount()).isEqualTo(1);
    }

    @Test
    public void metersInfoEvents() throws Exception {
        when(event.getLevel()).thenReturn(Level.INFO);

        appender.append(event);

        assertThat(registry.meter(METRIC_NAME_PREFIX + ".all").getCount()).isEqualTo(1);
        assertThat(registry.meter(METRIC_NAME_PREFIX + ".info").getCount()).isEqualTo(1);
    }

    @Test
    public void metersWarnEvents() throws Exception {
        when(event.getLevel()).thenReturn(Level.WARN);

        appender.append(event);

        assertThat(registry.meter(METRIC_NAME_PREFIX + ".all").getCount()).isEqualTo(1);
        assertThat(registry.meter(METRIC_NAME_PREFIX + ".warn").getCount()).isEqualTo(1);
    }

    @Test
    public void metersErrorEvents() throws Exception {
        when(event.getLevel()).thenReturn(Level.ERROR);

        appender.append(event);

        assertThat(registry.meter(METRIC_NAME_PREFIX + ".all").getCount()).isEqualTo(1);
        assertThat(registry.meter(METRIC_NAME_PREFIX + ".error").getCount()).isEqualTo(1);
    }
}
