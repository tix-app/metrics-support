package tix.metrics.codahale.collector;

import com.codahale.metrics.Gauge;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ThreadStatesGaugeSetTest extends AbstractMetricsTest {
    private final ThreadMXBean threads = mock(ThreadMXBean.class);
    private final ThreadDeadlockDetector detector = mock(ThreadDeadlockDetector.class);
    private final long[] ids = new long[]{ 1, 2, 3 };

    private final ThreadInfo newThread = mock(ThreadInfo.class);
    private final ThreadInfo runnableThread = mock(ThreadInfo.class);
    private final ThreadInfo blockedThread = mock(ThreadInfo.class);
    private final ThreadInfo waitingThread = mock(ThreadInfo.class);
    private final ThreadInfo timedWaitingThread = mock(ThreadInfo.class);
    private final ThreadInfo terminatedThread = mock(ThreadInfo.class);

    private final Set<String> deadlocks = new HashSet<>();
    private ThreadStatesGaugeSet gauges;

    @Before
    public void setUp() throws Exception {
        gauges = new ThreadStatesGaugeSet(threads, detector);

        deadlocks.add("yay");

        Mockito.when(newThread.getThreadState()).thenReturn(Thread.State.NEW);
        Mockito.when(runnableThread.getThreadState()).thenReturn(Thread.State.RUNNABLE);
        Mockito.when(blockedThread.getThreadState()).thenReturn(Thread.State.BLOCKED);
        Mockito.when(waitingThread.getThreadState()).thenReturn(Thread.State.WAITING);
        Mockito.when(timedWaitingThread.getThreadState()).thenReturn(Thread.State.TIMED_WAITING);
        Mockito.when(terminatedThread.getThreadState()).thenReturn(Thread.State.TERMINATED);

        Mockito.when(threads.getAllThreadIds()).thenReturn(ids);
        Mockito.when(threads.getThreadInfo(ids, 0)).thenReturn(new ThreadInfo[]{
                newThread, runnableThread, blockedThread,
                waitingThread, timedWaitingThread, terminatedThread
        });

        Mockito.when(threads.getThreadCount()).thenReturn(12);
        Mockito.when(threads.getDaemonThreadCount()).thenReturn(13);

        Mockito.when(detector.getDeadlockedThreads()).thenReturn(deadlocks);
    }

    @Test
    public void hasASetOfGauges() throws Exception {
        assertThat(gauges.getMetrics().keySet())
                .containsOnly(
                        "jvm.thread.daemon.count",
                        "jvm.thread.waiting.count",
                        "jvm.thread.new.count",
                        "jvm.thread.runnable.count",
                        "jvm.thread.blocked.count",
                        "jvm.thread.timed_waiting.count",
                        "jvm.thread.terminated.count",
                        "jvm.thread.count",
                        "jvm.thread.deadlocks");
    }

    @Test
    public void hasAGaugeForEachThreadState() throws Exception {
        assertThat(((Gauge) gauges.getMetrics().get("jvm.thread.new.count")).getValue())
                .isEqualTo(1);

        assertThat(((Gauge) gauges.getMetrics().get("jvm.thread.runnable.count")).getValue())
                .isEqualTo(1);

        assertThat(((Gauge) gauges.getMetrics().get("jvm.thread.blocked.count")).getValue())
                .isEqualTo(1);

        assertThat(((Gauge) gauges.getMetrics().get("jvm.thread.waiting.count")).getValue())
                .isEqualTo(1);

        assertThat(((Gauge) gauges.getMetrics().get("jvm.thread.timed_waiting.count")).getValue())
                .isEqualTo(1);

        assertThat(((Gauge) gauges.getMetrics().get("jvm.thread.terminated.count")).getValue())
                .isEqualTo(1);
    }

    @Test
    public void hasAGaugeForTheNumberOfThreads() throws Exception {
        assertThat(((Gauge) gauges.getMetrics().get("jvm.thread.count")).getValue())
                .isEqualTo(12);
    }

    @Test
    public void hasAGaugeForTheNumberOfDaemonThreads() throws Exception {
        assertThat(((Gauge) gauges.getMetrics().get("jvm.thread.daemon.count")).getValue())
                .isEqualTo(13);
    }

    @Test
    public void hasAGaugeForAnyDeadlocks() throws Exception {
        assertThat(((Gauge) gauges.getMetrics().get("jvm.thread.deadlocks")).getValue())
                .isEqualTo(deadlocks);
    }

/*
    @Test
    public void hasAGaugeForAnyDeadlockCount() throws Exception {
        assertThat(((Gauge) gauges.getMetrics().get("jvm.thread.deadlocks.count")).getValue())
                .isEqualTo(1);
    }
*/

    @Test
    public void autoDiscoversTheMXBeans() throws Exception {
        final ThreadStatesGaugeSet set = new ThreadStatesGaugeSet();
        assertThat(((Gauge) set.getMetrics().get("jvm.thread.count")).getValue())
                .isNotNull();
        assertThat(((Gauge) set.getMetrics().get("jvm.thread.deadlocks")).getValue())
                .isNotNull();
    }
}