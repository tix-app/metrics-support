package tix.metrics.namespace;

import org.junit.Test;

import static org.junit.Assert.*;

public class FixedNamespaceResolverTest {

    @Test
    public void testNamespace() throws Exception {
        FixedNamespaceResolver resolver = new FixedNamespaceResolver("my-fixed-namespace");
        assertEquals("my-fixed-namespace", resolver.namespace());
    }
}