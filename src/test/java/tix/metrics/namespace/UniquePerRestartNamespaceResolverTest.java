package tix.metrics.namespace;

import org.junit.Before;
import org.junit.Test;
import tix.test.autowire.AutowiringTest;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;

import static org.junit.Assert.*;

public class UniquePerRestartNamespaceResolverTest extends AutowiringTest {
    @Test
    public void testNamespace() throws Exception {
        Clock clock = Clock.fixed(Instant.ofEpochMilli(0), ZoneId.of("UTC"));
        registerBean("clock", clock);

        NamespaceResolver delegate = new FixedNamespaceResolver("delegate");
        UniquePerRestartNamespaceResolver uniqueResolver = new UniquePerRestartNamespaceResolver(delegate);
        String namespace1 = uniqueResolver.namespace();
        assertEquals("delegate-19700101-0000", namespace1);

        registerBean("clock", Clock.offset(clock, Duration.ofMinutes(5)));
        uniqueResolver = new UniquePerRestartNamespaceResolver(delegate);
        String namespace2 = uniqueResolver.namespace();
        assertEquals("delegate-19700101-0005", namespace2);
    }
}