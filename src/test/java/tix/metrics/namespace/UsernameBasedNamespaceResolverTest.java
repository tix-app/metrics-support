package tix.metrics.namespace;

import org.junit.Test;

import static org.junit.Assert.*;

public class UsernameBasedNamespaceResolverTest {

    @Test
    public void testNamespace() throws Exception {
        System.clearProperty("user.name");
        UsernameBasedNamespaceResolver resolver = new UsernameBasedNamespaceResolver();
        assertEquals("unknown", resolver.namespace());

        System.setProperty("user.name", "Alice");
        resolver = new UsernameBasedNamespaceResolver();
        assertEquals("Alice", resolver.namespace());
    }
}